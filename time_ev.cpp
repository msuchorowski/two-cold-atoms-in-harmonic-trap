#include "fun.h"


//time evolution of the system


//calculate time evolution coefficents 
void time_evolution_operator_different_atoms_coeff(gsl_vector_complex eigenvectors_init, gsl_matrix_complex eigenvectors_fin, gsl_vector Eigenvalues_fin, double t0, double t1, double dt, double s1, int basis_size, int M_init, vector<State> basis, double B1, double B2, double J1, double J2, int ind, int ind_end){

        cout << "te function" << endl;
        int N = basis.size();

        int Nt=int((t1-t0)/dt);
        gsl_complex Tab01[Nt];
        gsl_complex Tab02[Nt];
        gsl_complex Tab03[Nt];
        for(int it=0; it<Nt; it++ ){
        //    cout << Nt << " " << it << endl;
            Tab01[it]=gsl_complex_rect(0,0);
            Tab02[it]=gsl_complex_rect(0,0);
            Tab03[it]=gsl_complex_rect(0,0);
            }

        bool print_info = false;
        bool gen = false;

        int index1=0;
        cout << "start the loops" << endl;
        cout << "Basis size: " << N << endl;
        int i = ind;
                gsl_complex a=scalar_product_different_atoms(eigenvectors_init, eigenvectors_fin, i, N); //scalar product <psi_i|psi_init>
                    if(abs(gsl_complex_abs(a))>1e-10 || gen){ //if a==0 then abc*exp=0
                            for(int j = ind_end+1; j<N; j++){  // loop over all eigenstates for H_fin (for each spin state N eigenstates with different harmonic states composition)
                                   gsl_complex b=scalar_product_different_atoms(eigenvectors_init, eigenvectors_fin, j, N); //scalar product <psi_j|psi_init>
                                    if(abs(gsl_complex_abs(b))>1e-10 || gen){ //if b==0 then abc*exp=0
                                        double Ei=gsl_vector_get(&Eigenvalues_fin,i); //energy Ei
                                        double Ej=gsl_vector_get(&Eigenvalues_fin,j); //energy Ej
  
                                        gsl_complex c1=exp_value_different_atoms(eigenvectors_fin,i,j,N,basis,1); // exp value <psi_i|operator|psi_j>
                                        gsl_complex c2=exp_value_different_atoms(eigenvectors_fin,i,j,N,basis,2); // exp value <psi_i|operator|psi_j>
                                        gsl_complex c3=exp_value_different_atoms(eigenvectors_fin,i,j,N,basis,3); // exp value <psi_i|operator|psi_j>

                                 
                                        gsl_complex abc1 = gsl_complex_mul(gsl_complex_mul(a,b),c1); // a*b*c
                                        gsl_complex abc2 = gsl_complex_mul(gsl_complex_mul(a,b),c2); // a*b*c
                                        gsl_complex abc3 = gsl_complex_mul(gsl_complex_mul(a,b),c3); // a*b*c

                                        gsl_complex ab = gsl_complex_mul(a,b); // a*b*c

                                        string file_name = "data/coeff/"+to_string(ind);
                                        ofstream file;
                                        file.open (file_name, std::ofstream::out|std::ofstream::app);
                                        file << i << " " << j << " " << GSL_REAL(abc1) << " " << GSL_IMAG(abc1) << " " << GSL_REAL(abc2) << " " << GSL_IMAG(abc2) << " " << GSL_REAL(abc3) << " " << GSL_IMAG(abc3) << " " << Ei << " " << Ej << endl;
                                        file.close();          

                                }
                                else{
                                    string file_name = "data/coeff/"+to_string(ind);
                                    ofstream file;
                                    file.open (file_name, std::ofstream::out|std::ofstream::app);
                                    file << i << " " << j << " " << 0 << " " << 0 << " " << 0 << " " << 0 << " " << 0 << " " << 0 << " " << 0 << " " << 0 << endl;
                                    file.close();  
                                }
                            }
                        
                    }
                    else{               
                        string file_name = "data/coeff/"+to_string(ind);
                        ofstream file;
                        file.open (file_name, std::ofstream::out|std::ofstream::app);
                        for(int j = ind_end+1; j<N; j++)
                            file << i << " " << j << " " << 0 << " " << 0 << " " << 0 << " " << 0 << " " << 0 << " " << 0 << " " << 0 << " " << 0 << endl;
                        file.close();  
                        }
                
                        string file_name = "log.log";
                        ofstream file;
                        file.open (file_name, std::ofstream::out|std::ofstream::app);
                        file << "Ind " << ind << " finished." << endl;
                        file.close();  
        
}


void time_evolution_operator_different_atoms(double t0, double t1, double dt, vector <State> basis, int basis_size){
        cout << "time ev" << endl;
        int N = basis.size();

        int Nt=int((t1-t0)/dt);
        gsl_complex Tab01[Nt];
        gsl_complex Tab02[Nt];
        gsl_complex Tab03[Nt];
        for(int it=0; it<Nt; it++ ){
        //    cout << Nt << " " << it << endl;
            Tab01[it]=gsl_complex_rect(0,0);
            Tab02[it]=gsl_complex_rect(0,0);
            Tab03[it]=gsl_complex_rect(0,0);
            }

        cout << "start loop"<< endl;

        for(int i = 0; i<N; i++){ 
            string path = "data/coeff/"+to_string(i);
            cout << path << endl;
            std::ifstream infile(path);
            double ii,jj;
            double abc1_real, abc1_imag, abc2_real, abc2_imag, abc3_real, abc3_imag;
            double Ei,Ej; 
            while (infile >> ii >> jj >> abc1_real >> abc1_imag >> abc2_real >> abc2_imag >> abc3_real >> abc3_imag >> Ei >> Ej)
            {
                double maxE=basis_size*2+0.5;
                if(Ei>maxE || Ej>maxE)
                    continue;
                gsl_complex abc1 = gsl_complex_rect(abc1_real,abc1_imag); // a*b*c
                gsl_complex abc2 = gsl_complex_rect(abc2_real,abc2_imag); // a*b*c
                gsl_complex abc3 = gsl_complex_rect(abc3_real,abc3_imag); // a*b*c

            
                for(int it=0; it<Nt; it++ ){
                    double t=t0+it*dt;
                    gsl_complex arg = gsl_complex_mul_real(gsl_complex_rect(0,1),-t*(Ej-Ei)); //-i(Ei-Ej)*t
                    gsl_complex abc_exp1=gsl_complex_mul(abc1,gsl_complex_exp(arg)); // abc*exp(arg) 
                    gsl_complex abc_exp2=gsl_complex_mul(abc2,gsl_complex_exp(arg)); // abc*exp(arg)           
                    gsl_complex abc_exp3=gsl_complex_mul(abc3,gsl_complex_exp(arg)); // abc*exp(arg)           

                    Tab01[it]=gsl_complex_add(Tab01[it],abc_exp1); //update exp_m1
                    Tab02[it]=gsl_complex_add(Tab02[it],abc_exp2); //update exp_m1
                    Tab03[it]=gsl_complex_add(Tab03[it],abc_exp3); //update exp_m1
                }
            }
        }


    double start_O;
    for(int it=0; it<Nt; it++ ){
        
        double t=t0+it*dt;
        cout << "t=" << t << endl;
        cout << "<O> = ";
        cout <<  GSL_REAL(Tab01[it]) << " " <<  GSL_REAL(Tab02[it]) << " " << GSL_REAL(Tab03[it]) << endl;
        string file_name = "data/TimeEv_RealAtoms_Cr_Er_ubB=0.5";
        ToFileTime(file_name,t,Tab01[it],Tab02[it],Tab03[it]); //save to file (tofile.cpp)
    }
        
}

//calculate time evolution of the operator for indistinguishable atoms
void time_evolution_operator(vector<gsl_vector_complex> eigenvectors_init, vector<gsl_matrix_complex> eigenvectors_fin, vector<gsl_vector> Eigenvalues_fin, double t0, double t1, double dt, double s1, int basis_size, int M_init, vector<vector<State>> basis, double B1, double B2, double J1, double J2){

        cout << "te function" << endl;
        int N = basis_size;

        int Nt=int((t1-t0)/dt);
        gsl_complex Tab01[Nt];
        gsl_complex Tab02[Nt];
        gsl_complex Tab03[Nt];
        for(int it=0; it<Nt; it++ ){
        //    cout << Nt << " " << it << endl;
            Tab01[it]=gsl_complex_rect(0,0);
            Tab02[it]=gsl_complex_rect(0,0);
            Tab03[it]=gsl_complex_rect(0,0);
            }

        bool print_info = false;
        bool gen = false;

        int index1=0;
        cout << "start the loops" << endl;
        for(int S1=s1*2; S1>=0; S1-=1){
            int index2=0;
            cout << "S1 " << S1 << endl;
            for(int  S2=s1*2; S2>=0; S2-=1){
                cout << "S2 " << S2 << endl;
                if(S1!=S2) {
                    index2++;
                    continue;
                    }
                for(int i = 0; i<N*(2*S1+1); i++){ // loops over all eigenstates for H_fin (for each M_s spin state N*N_S eigenstates with different harmonic states composition)
                    gsl_complex a=scalar_product(eigenvectors_init, eigenvectors_fin, index1,  i, N, S1, s1); //scalar product <psi_i|psi_init>

                    if(abs(gsl_complex_abs(a))>1e-10 || gen){ //if a==0 then abc*exp=0
                       // cout << "S1 " << S1 << " p " << i << " scal1=" << gsl_complex_abs(a) << endl;
                            for(int j = 0; j<N*(2*S2+1); j++){  // loop over all eigenstates for H_fin (for each spin state N eigenstates with different harmonic states composition)

                                    //printing additional information like eigenstates and inital state (only if print_info==true)
                                    if(print_info){ //gsl_complex_abs2(c)>0.000001 && 
                                        cout << "index1 index2  " << index1 << "==" << index2 << endl; 
                                        cout << "p q = " << i << " " << j << endl; 
                                        cout << "State A: ";
                                        for(int q=0; q<N*(2*S2+1); q++){
                                            cout << "S=" << S1 << " M=" << basis[index1][q].M << " n=" << basis[index1][q].n << " " <<  GSL_REAL(gsl_matrix_complex_get(&eigenvectors_fin[index1],q,i)) << endl;
                                        }
                                        cout << endl;
                                       cout << "State A*: ";
                                        for(int q=0; q<N*(2*S2+1); q++){
                                            cout << "S=" << S1 << " M=" << basis[index1][q].M << " n=" << basis[index1][q].n << " " <<  GSL_REAL(gsl_complex_conjugate(gsl_matrix_complex_get(&eigenvectors_fin[index1],q,i))) << " " << GSL_REAL((gsl_matrix_complex_get(&eigenvectors_fin[index2],q,j))) << " " << GSL_REAL(gsl_vector_complex_get(&eigenvectors_init[index1],q)) << endl;
                                        }
                                        cout << endl;
                                        cout << "State B: ";
                                        for(int q=0; q<N*(2*S2+1); q++){
                                            cout << "S=" << S2 << " M=" << basis[index2][q].M << " n=" << basis[index2][q].n << " " <<  GSL_REAL(gsl_complex_conjugate(gsl_matrix_complex_get(&eigenvectors_fin[index2],q,j))) << endl;
                                        }
                                        cout << endl;
                                    }
                                    gsl_complex b=scalar_product(eigenvectors_init, eigenvectors_fin, index2,  j, N, S2, s1); //scalar product <psi_j|psi_init>
                                    if(abs(gsl_complex_abs(b))>1e-10 || gen){ //if b==0 then abc*exp=0
                                        //cout << "M2 " << M2 << " q " << j << " scal2=" << gsl_complex_abs(b) << endl; 
                                        //cout << "Non-zero scalars " << "M1=" << M1 << "M2=" << M2 << endl;  
                                        double Ei=gsl_vector_get(&Eigenvalues_fin[index1],i); //energy Ei
                                        double Ej=gsl_vector_get(&Eigenvalues_fin[index2],j); //energy Ej
                                        double maxE=basis_size*2+0.5;
                                        
                                        if(Ei > maxE || Ej > maxE) 
                                            continue;
                                        

                                        gsl_complex c1=exp_value(eigenvectors_fin,i,j,index1,index2,N,s1,S1,S2,basis,1); // exp value <psi_i|operator|psi_j>
                                        gsl_complex c2=exp_value(eigenvectors_fin,i,j,index1,index2,N,s1,S1,S2,basis,2); // exp value <psi_i|operator|psi_j>
                                        gsl_complex c3=exp_value(eigenvectors_fin,i,j,index1,index2,N,s1,S1,S2,basis,3); // exp value <psi_i|operator|psi_j>

                                        gsl_complex M1=exp_value(eigenvectors_fin,i,i,index1,index1,N,s1,S1,S1,basis,3);
                                        gsl_complex M2=exp_value(eigenvectors_fin,j,j,index2,index2,N,s1,S1,S1,basis,3);

                                        gsl_complex abc1 = gsl_complex_mul(gsl_complex_mul(a,b),c1); // a*b*c
                                        gsl_complex abc2 = gsl_complex_mul(gsl_complex_mul(a,b),c2); // a*b*c
                                        gsl_complex abc3 = gsl_complex_mul(gsl_complex_mul(a,b),c3); // a*b*c

                                        gsl_complex ab = gsl_complex_mul(a,b); // a*b*c


                                        //if(abs(gsl_complex_abs(ab))>1e-10 && S1==S2)
                                        //    cout << S1 << " " << S2 << " " << i << " " << j << " " << abs(Ei-Ej) << " " << gsl_complex_abs(c3) << endl;
                                        string file_name = "data/TimeEv_Analysis_States_S="+to_string(S1)+"_s="+to_string(s1);
                                        ofstream file;
                                        file.open (file_name, std::ofstream::out|std::ofstream::app);
                                        file << i << " " << j << " " << GSL_REAL(ab) << " " << gsl_complex_abs(c3) << " " << abs(Ei-Ej) << endl;
                                      //  cout << "c1*c2=" << GSL_REAL(ab) << " <S_z>=" << gsl_complex_abs(c3) << " DE=" << abs(Ei-Ej) << " Ei=" << Ei << " Ej=" << Ej << " <M>_1=" << GSL_REAL(M1) << " <M>_2=" << GSL_REAL(M2) << endl;

                                        file.close();
                                        
                                        for(int it=0; it<Nt; it++ ){
                                        double t=t0+it*dt;
                                        gsl_complex arg = gsl_complex_mul_real(gsl_complex_rect(0,1),-t*(Ej-Ei)); //-i(Ei-Ej)*t
                                        gsl_complex abc_exp1=gsl_complex_mul(abc1,gsl_complex_exp(arg)); // abc*exp(arg) 
                                        gsl_complex abc_exp2=gsl_complex_mul(abc2,gsl_complex_exp(arg)); // abc*exp(arg)           
                                        gsl_complex abc_exp3=gsl_complex_mul(abc3,gsl_complex_exp(arg)); // abc*exp(arg)           

                                        Tab01[it]=gsl_complex_add(Tab01[it],abc_exp1); //update exp_m1
                                        Tab02[it]=gsl_complex_add(Tab02[it],abc_exp2); //update exp_m1
                                        Tab03[it]=gsl_complex_add(Tab03[it],abc_exp3); //update exp_m1

                                        }
            

                                }
                            }
                        
                    }
                }
                index2++; //index1,index2 points to index of the eigenvectors_fin vector with given spin_state
                }
                index1++;
            }

    double start_O;
    for(int it=0; it<Nt; it++ ){
        
        double t=t0+it*dt;
        cout << "t=" << t << endl;
        cout << "<O> = ";
        cout <<  GSL_REAL(Tab01[it]) << " " <<  GSL_REAL(Tab02[it]) << " " << GSL_REAL(Tab03[it]) << endl;
        string file_name = "data/TimeEv_H0_Mz_s="+to_string(s1)+"_Bz="+to_string(B1*uB)+"_Bx="+to_string(B2*uB)+"_J="+to_string(J2)+"_init="+to_string(M_init);
        ToFileTime(file_name,t,Tab01[it],Tab02[it],Tab03[it]); //save to file (tofile.cpp)


    }
        
}

//calcluate scalar product (overlap) of initial state and eigenstates of Hamiltonian for indistinguishable atoms
gsl_complex scalar_product(vector<gsl_vector_complex> eigenvector_init, vector<gsl_matrix_complex> eigenvectors_fin, int ind_spin,  int ind_eigenstate, int basis_size, int S, double s1){
    gsl_complex sp=gsl_complex_rect(0,0);
                /*for(int i = 0; i<basis_size; i++){ //loop over harmonic components
                    gsl_complex a1=gsl_complex_conjugate(gsl_matrix_complex_get(&eigenvectors_fin[ind_spin],i,ind_eigenstate)); //take conjugate of coefficent from eigenvector for i-th harmonic component
                    gsl_complex a2=gsl_vector_complex_get(&eigenvector_init[ind_spin],i); //take coefficient from initial eigenvector for i-th harmonic component
                    sp=gsl_complex_add(sp,gsl_complex_mul(a1,a2));

                }*/
                for(int M = -S; M<=S; M++){
                    for(int q = 0; q<basis_size; q++){
                        gsl_complex a1=gsl_complex_conjugate(gsl_matrix_complex_get(&eigenvectors_fin[ind_spin],int(M+S)*basis_size+q,ind_eigenstate)); //take conjugate of coefficent from eigenvector for n_init-th harmonic component
                        gsl_complex a2=gsl_vector_complex_get(&eigenvector_init[ind_spin],int(M+S)*basis_size+q); //take coefficient from initial eigenvector for n_init-th harmonic component
                        //if(gsl_complex_abs2(a2)>1e-5)
                         //  cout << "M=" << M << " S=" << S << " q=" << q << " a1=" << gsl_complex_abs2(a1) << " " << "a2 " << gsl_complex_abs2(a2) << endl;
                        sp=gsl_complex_add(sp,gsl_complex_mul(a1,a2));
                    //   }
                    }
                    }
    return sp;

}

//calcluate scalar product (overlap) of initial state and eigenstates of Hamiltonian for different atoms
gsl_complex scalar_product_different_atoms(gsl_vector_complex eigenvector_init, gsl_matrix_complex eigenvectors_fin, int ind_eigenstate, int basis_size){
    gsl_complex sp=gsl_complex_rect(0,0);
    for(int q = 0; q<basis_size; q++){
            gsl_complex a1=gsl_complex_conjugate(gsl_matrix_complex_get(&eigenvectors_fin,q,ind_eigenstate)); //take conjugate of coefficent from eigenvector for n_init-th harmonic component
            gsl_complex a2=gsl_vector_complex_get(&eigenvector_init,q); //take coefficient from initial eigenvector for n_init-th harmonic component
                sp=gsl_complex_add(sp,gsl_complex_mul(a1,a2));
                    }
    return sp;
}

//calcluate non-diagonal expectation value (matrix elements) of operator between initial state and eigenstates of Hamiltonian for indistinguishable atoms
gsl_complex exp_value(vector<gsl_matrix_complex> eigenvectors, int ind_eigenstate1, int ind_eigenstate2, int ind_spin1, int ind_spin2, int basis_size, double s1, int S1, int S2, vector<vector<State>> basis, int flag){

    gsl_complex ev=gsl_complex_rect(0,0);
    //basis_size*(s1*2-abs(M1)+1)
    vector<State> states1=basis[2*s1-S1];
    vector<State> states2=basis[2*s1-S2];
    int N1=states1.size();
    int N2=states2.size();
    //cout << "SIZES for M1=" << M1 << " N1=" << N1 << " and M2=" << M2 << " N2=" << N2 << endl;   
    

    for(int q=0; q<N1; q++)// alpha and alpha prime from Ania's notes
        for(int p=0; p<N2; p++){
        gsl_complex a1=gsl_complex_conjugate(gsl_matrix_complex_get(&eigenvectors[ind_spin1],q,ind_eigenstate1)); //take conjugate of coefficent from eigenvector for q-th harmonic component
        gsl_complex a2=gsl_matrix_complex_get(&eigenvectors[ind_spin2],p,ind_eigenstate2); //take coefficent from eigenvector for q-th harmonic component
        gsl_complex aa = gsl_complex_mul(a1,a2);
        if (flag==1)
            ev=gsl_complex_add( ev, gsl_complex_mul_real( aa,exp_value_basis_m1(states1[q], states2[p]) ) ); //add aa*<alpha|operator|alpha'>
        else if (flag==2)
            ev=gsl_complex_add( ev, gsl_complex_mul_real( aa,exp_value_basis_m2(states1[q], states2[p]) ) ); //add aa*<alpha|operator|alpha'>
        else if (flag==3){
            ev=gsl_complex_add( ev, gsl_complex_mul_real( aa,exp_value_basis_M(states1[q], states2[p]) ) ); //add aa*<alpha|operator|alpha'>
          /*  if(states1[p].n==states2[q].n && states1[p].M==states2[q].M){
                cout << "Comp " << p << " " << q << endl;
                cout << states1[p].n << " " <<  states1[p].M << " " << exp_value_basis_M(states1[q], states2[p])  << " "  << GSL_REAL(a1) << " " << GSL_REAL(a2) << " " << GSL_REAL(aa) << " " << GSL_REAL(gsl_complex_mul_real( aa,exp_value_basis_M(states1[q], states2[p]) )) << " " << GSL_REAL(ev) << endl;
                }*/
            }
        
        
        //cout << "State1: ";
        //states1[q].PrintQuantumNumbers();
        //cout << "State2: ";
        //states2[p].PrintQuantumNumbers();
    }
    return ev;
}

//calcluate non-diagonal expectation value (matrix elements) of operator between initial state and eigenstates of Hamiltonian for different atoms
gsl_complex exp_value_different_atoms(gsl_matrix_complex eigenvectors, int ind_eigenstate1, int ind_eigenstate2, int basis_size, vector<State> basis, int flag){

    gsl_complex ev=gsl_complex_rect(0,0);
    //basis_size*(s1*2-abs(M1)+1)
    
    int N=basis.size();
    

    for(int q=0; q<N; q++)// alpha and alpha prime from Ania's notes
        for(int p=0; p<N; p++){
        gsl_complex a1=gsl_complex_conjugate(gsl_matrix_complex_get(&eigenvectors,q,ind_eigenstate1)); //take conjugate of coefficent from eigenvector for q-th harmonic component
        gsl_complex a2=gsl_matrix_complex_get(&eigenvectors,p,ind_eigenstate2); //take coefficent from eigenvector for q-th harmonic component
        gsl_complex aa = gsl_complex_mul(a1,a2);
        if (flag==1)
            ev=gsl_complex_add( ev, gsl_complex_mul_real( aa,exp_value_basis_m1(basis[q], basis[p]) ) ); //add aa*<alpha|operator|alpha'>
        else if (flag==2)
            ev=gsl_complex_add( ev, gsl_complex_mul_real( aa,exp_value_basis_m2(basis[q], basis[p]) ) ); //add aa*<alpha|operator|alpha'>
        else if (flag==3){
            ev=gsl_complex_add( ev, gsl_complex_mul_real( aa,exp_value_basis_M(basis[q], basis[p]) ) ); //add aa*<alpha|operator|alpha'>
            }
    }
    return ev;
}


//calculate exp value of operator from basis states <n'|<S'M'| S_1z | SM > | n>
double exp_value_basis_m1(State state1, State state2){
    double exp=0;

    if(state1.n==state2.n){
        for(double m1=-state1.s1; m1<=state1.s1; m1++) // loops over m1,m2 states
            for(double m2=-state1.s2; m2<=state1.s2; m2++){
                //cout <<  "m1=" << m1 << " m2=" << m2 << " C1=" << CG_coff(state1.s1, state1.s2, m1, m2, state1.S, state1.M) << " C2=" << CG_coff(state2.s1, state2.s2, m1, m2, state2.S, state2.M) << " C1*C2*m1=" << CG_coff(state1.s1, state1.s2, m1, m2, state1.S, state1.M)*CG_coff(state2.s1, state2.s2, m1, m2, state2.S, state2.M)*m1 << endl;
                exp+=CG_coff(state1.s1, state1.s2, m1, m2, state1.S, state1.M)*CG_coff(state2.s1, state2.s2, m1, m2, state2.S, state2.M)*m1;
            }
    }
    //exp=exp*gsl_sf_hermite_phys(state1.n, 0)*gsl_sf_hermite_phys(state2.n, 0);
    return exp;
}

//calculate exp value of operator from basis states <n'|<S'M'| S_2z | SM > | n>
double exp_value_basis_m2(State state1, State state2){
    double exp=0;

    if(state1.n==state2.n){
        for(double m1=-state1.s1; m1<=state1.s1; m1++) // loops over m1,m2 states
            for(double m2=-state1.s2; m2<=state1.s2; m2++){
                //cout <<  "m1=" << m1 << " m2=" << m2 << " C1=" << CG_coff(state1.s1, state1.s2, m1, m2, state1.S, state1.M) << " C2=" << CG_coff(state2.s1, state2.s2, m1, m2, state2.S, state2.M) << " C1*C2*m1=" << CG_coff(state1.s1, state1.s2, m1, m2, state1.S, state1.M)*CG_coff(state2.s1, state2.s2, m1, m2, state2.S, state2.M)*m1 << endl;
                exp+=CG_coff(state1.s1, state1.s2, m1, m2, state1.S, state1.M)*CG_coff(state2.s1, state2.s2, m1, m2, state2.S, state2.M)*m2;
            }
    }
    //exp=exp*gsl_sf_hermite_phys(state1.n, 0)*gsl_sf_hermite_phys(state2.n, 0);
    return exp;
}



//calculate exp value of operator from basis states <n'|<S'M'| S_z | SM > | n>
double exp_value_basis_M(State state1, State state2){
    double exp=0;

    if(state1.n==state2.n && state1.S==state2.S && state1.M==state2.M ){
        exp=state1.M;
            
    }
    //exp=exp*gsl_sf_hermite_phys(state1.n, 0)*gsl_sf_hermite_phys(state2.n, 0);
    return exp;
}

//calculate exp value of operator from basis states <n'|<S'M'| S^2 | SM > | n>
double exp_value_basis_S2(State state1, State state2){
    double exp=0;

    if(state1.n==state2.n && state1.S==state2.S && state1.M==state2.M){
        exp=state1.S*(state1.S+1);
            
    }
    return exp;
}


// transform the initial product state to the |SM> basis
vector<gsl_vector_complex> initial_state(double m1_init, double m2_init, int harmonic_state, double s1, int basis_size){

    int n=(2*s1+1)*(2*s1+1); //size of the state vector in |m1,m2> basis
    gsl_vector_complex* V_m1m2 = gsl_vector_complex_calloc(n);

    int index=0;
    //generate state vector in |m1,m2> basis for given initial m1,m2 and harmonic state
    for(double m1=-s1; m1<=s1; m1++) // loop over m1,m2
        for(double m2=-s1; m2<=s1; m2++){
            if(abs(m1-m1_init)<10e-3 && abs(m2-m2_init)<10e-3){
                gsl_vector_complex_set(V_m1m2,index,gsl_complex_rect(1,0));
                }
            index++;

        }

    gsl_matrix* invCG = inv_CG_matrix(s1,s1,n); //generate inverse C-G coefficients matrix (CG.cpp)
    gsl_matrix_complex* cinvCG = MtoComplexM(invCG,n); //rewrite matrix to complex variables (CG.cpp)

    gsl_vector_complex* V_SM = gsl_vector_complex_calloc(n);

    gsl_blas_zgemv(CblasNoTrans, gsl_complex_rect(1,0), cinvCG, V_m1m2, gsl_complex_rect(0,0), V_SM); // calculate product V_SM=cinvCG*V_m1m2

    //print V_m1m2 and V_SM vectors 
    double sum=0;
    for(int i=0; i<n; i++){
        cout << GSL_REAL(gsl_vector_complex_get(V_m1m2,i)) << " ";
        sum+=gsl_complex_abs2(gsl_vector_complex_get(V_m1m2,i));
        }
    cout << endl;
    cout << sum << endl;
    sum=0;
    for(int i=0; i<n; i++){
        cout << GSL_REAL(gsl_vector_complex_get(V_SM,i)) << " ";
        sum+=gsl_complex_abs2(gsl_vector_complex_get(V_SM,i));
        }
    cout << endl;
    cout << sum << endl;


    //rewrite V_SM vector to state in form used in other parts of the program
    // (Smax (n1, n3, n5,..) Smax-1 (n2,n4,n6,...), ... , 0 (...))
    //inside of V_SM there coeff are in order (M_max (Smax, Smax-1,...,M_max), M_max-1 (...), 0 (Smax,...,0), -1 (Smax,...,1),...)
    //int n_ms=(s1+s2)*2+1;

    vector<gsl_vector_complex> InitState;
    for(int m=s1*2; m>=-(s1*2); m-=1){
        double N_m=0;
        for(int nm=s1*2; nm>m; nm-=1){
            N_m+=2*s1-abs(nm)+1;
            }
        gsl_vector_complex* state = gsl_vector_complex_calloc(basis_size*(s1*2-abs(m)+1));
        for(int s=s1*2; s>=abs(m); s-=1){
            if(s%2==harmonic_state%2) // there are no eigenstates with even S and odd n (and odd S and even n), therefore we exclude them from initial state too (that part of initial state have no overlap with eigenstates)
                gsl_vector_complex_set(state, (s1*2-s)*basis_size+(harmonic_state/2), gsl_vector_complex_get(V_SM,N_m+(s1*2-s))); // (harmonic_state/2) gives label of harmonic_state of specific symmetry (eg. n=1 is 0th antisymmetric state, n=4 is 2nd symmetric state)
            else
                gsl_vector_complex_set(state, (s1*2-s)*basis_size+(harmonic_state/2), gsl_vector_complex_get(V_SM,N_m+(s1*2-s)));
        }
        InitState.push_back(*state);
    }
  //for(double S=s1+s2; S>=0; S-=1) 
  //      for(double M=S; M>=-S; M-=1){
    //print full eigenvector
    cout << "Full vector" << endl;
    for(int i=0;i<s1*2*2+1;i++){
        for(int j=0;j<InitState[i].size;j++)
            cout << GSL_REAL(gsl_vector_complex_get(&InitState[i],j)) << " ";
         cout << endl;   
        }

    gsl_vector_complex_free(V_m1m2);
    gsl_vector_complex_free(V_SM);
    gsl_matrix_free(invCG);
    gsl_matrix_complex_free(cinvCG);    

    return InitState;

}

