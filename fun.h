#ifndef fun
#define fun


#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_sf_expint.h>
#include <gsl/gsl_sf_hermite.h>
#include <gsl/gsl_sort_vector.h>
#include <gsl/gsl_sf.h>
#include <gsl/gsl_complex_math.h>
#include <gsl/gsl_complex.h>
#include <gsl/gsl_linalg.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <utility>
#include <iomanip>
#include <algorithm>

using namespace std;
#define uB 0.5 //Bohr magneton
#define uN (0.5/1836.152) //nuclear magneton


//function defined in CG.cpp

double CG_coff(double s1, double s2, double m1, double m2, double S, double M);
gsl_matrix* CG_matrix(double s1, double s2, int N);
gsl_matrix* inv_CG_matrix(double s1, double s2, int N);
gsl_matrix_complex* MtoComplexM(gsl_matrix* M, int N);


//class and functions defined instate.cpp

class State // class State contain quantum numbers n,S,M and spins of single atoms s1,s2
{
    public:

        int n;
        double s1;
        double s2;
        double S;
        double M;

        State(int cn, double cs1, double cs2, double cS, double cM); // contructor
        void PrintQuantumNumbers(); // printing quantum numbers
};

vector<State> generate_basis(double s1, double s2, int basis_size, bool S_conserved, bool M_conserved, bool symmetry, int M=0, int S=0);
vector<State> generate_fermion_basis_SM(double s1, double s2, int basis_size, int S, int M);
vector<State> generate_boson_basis_SM(double s1, double s2, int basis_size, int S, int M);
vector<State> generate_nosymmetry_basis_SM(double s1, double s2, int basis_size, int S, int M);
vector<State> generate_fermion_basis_M(double s1, double s2, int basis_size, int M);
vector<State> generate_boson_basis_M(double s1, double s2, int basis_size, int M);
vector<State> generate_nosymmetry_basis_M(double s1, double s2, int basis_size, int M);
vector<State> generate_fermion_basis_S(double s1, double s2, int basis_size, int S);
vector<State> generate_boson_basis_S(double s1, double s2, int basis_size, int S);
vector<State> generate_nosymmetry_basis_S(double s1, double s2, int basis_size, int S);
vector<State> generate_fermion_basis(double s1, double s2, int basis_size);
vector<State> generate_boson_basis(double s1, double s2, int basis_size);
vector<State> generate_nosymmetry_basis(double s1, double s2, int basis_size);



//function defined in hamiltionan.cpp

gsl_matrix_complex*  generate_Hamiltonian(vector<State> basis_set, double J, double Bz, double Bx, double g1=2, double g2=2);
int DK(double a, double b);
pair<gsl_vector*,gsl_matrix_complex*> diagonalize_Hamiltonian(gsl_matrix_complex* H, int basis_size,vector<State> basis);
gsl_vector* expectation_value(gsl_matrix_complex* eigenvectors_t,vector<State> basis_set, double J, double Bz);
gsl_vector* basis_energy(vector<State> basis_set, double J, double Bz);
void print_ham(gsl_matrix_complex* H, vector<State> basis_set);
void print_eigen(gsl_matrix_complex* eigenvector, gsl_vector* eigenvalue, vector<State> basis_set,int basis_size);
double g(double S, double J, double L, double I, double F, double mu);

//function defined in tofile.cpp
void ToFile_SM(string file_name,  gsl_vector* Eigenvalues, gsl_matrix_complex* eigenvectors, double B, double J, int S, int M);
void ToFile_M(string file_name,  gsl_vector* Eigenvalues, gsl_matrix_complex* eigenvectors, double B, double J, int M, vector<State> basis, double s1);
void ToFile_S(string file_name,  gsl_vector* Eigenvalues, gsl_matrix_complex* eigenvectors, double B, double J, int S, vector<State> basis, double s1);
void ToFile(string file_name,  gsl_vector* Eigenvalues, gsl_matrix_complex* eigenvectors, double B, double J);
void ToFilePhase(string file_name, double B, double J, double S, double M, double domM=0);
void StateToFile(string file_name, double B, double J, double s, gsl_vector* Eigenvalues, gsl_matrix_complex* eigenvectors,vector <State> basis);
void StateToFile_Bx(string file_name, double B, double J, double s, gsl_vector* Eigenvalues, gsl_matrix_complex* eigenvectors,vector <State> basis);
void ToFileTime(string file_name, double t, gsl_complex m1, gsl_complex m2, gsl_complex M);
void ToFileOccupation(string file_name, gsl_vector* Eigenvalues, gsl_matrix_complex* eigenvectors, vector <State> basis, double Bz, double J, double M, double s1);
void ToFileBasisSize(string file_name, double basis_size, double E);
void ToFileEigenstate(gsl_matrix_complex* Eigenvectors, gsl_vector* Eigenvalues, string file_name, string file_name2);
pair<gsl_vector*,gsl_matrix_complex*> FromFileEigenstate(string file_name, string file_name2, int basis_size);


//function defined in time_ev.cpp
gsl_complex scalar_product(vector<gsl_vector_complex> eigenvector_init, vector<gsl_matrix_complex> eigenvectors_fin, int ind_spin,  int ind_eigenstate, int basis_size, int M, double s1);
gsl_complex exp_value(vector<gsl_matrix_complex> eigenvectors, int ind_eigenstate1, int ind_eigenstate2, int ind_spin1, int ind_spin2, int basis_size, double s1, int M1, int M2, vector<vector<State>> basis, int flag=0);
gsl_complex exp_value_different_atoms(gsl_matrix_complex eigenvectors, int ind_eigenstate1, int ind_eigenstate2, int basis_size, vector<State> basis, int flag);
gsl_complex scalar_product_different_atoms(gsl_vector_complex eigenvector_init, gsl_matrix_complex eigenvectors_fin, int ind_eigenstate, int basis_size);
void time_evolution_operator(vector<gsl_vector_complex> eigenvectors_init, vector<gsl_matrix_complex> eigenvectors_fin, vector<gsl_vector> Eigenvalues_fin, double t0, double t1, double dt, double s1, int basis_size, int M_init, vector<vector<State>> basis, double B1, double B2, double J1, double J2);
void time_evolution_operator_different_atoms(double t0, double t1, double dt, vector <State> basis, int basis_size);
void time_evolution_operator_different_atoms_coeff(gsl_vector_complex eigenvectors_init, gsl_matrix_complex eigenvectors_fin, gsl_vector Eigenvalues_fin, double t0, double t1, double dt, double s1, int basis_size, int M_init, vector<State> basis, double B1, double B2, double J1, double J2, int ind, int ind_end);
double exp_value_basis_m1(State state1, State state2);
double exp_value_basis_m2(State state1, State state2);
double exp_value_basis_S2(State state1, State state2);
double exp_value_basis_M(State state1, State state2);
vector<gsl_vector_complex> initial_state(double m1_init, double m2_init, int harmonic_state, double s1, int basis_size);



//function defined in calculations.cpp
void run_energy_spectrum_B(int basis_size, double s1, double s2, bool S_conserved, bool M_conserved);
void run_energy_spectrum_J(int basis_size, double s1, double s2, bool S_conserved, bool M_conserved);
void run_phase_diagram_Bz(int basis_size, double s1, double s2, double Jmin, double Jmax, int NJ, double Bmin, double Bmax, int NB);
void run_phase_diagram_Bx(int basis_size, double s1, double s2, double Jmin, double Jmax, int NJ, double Bmin, double Bmax, int NB);
void run_phase_diagram_Mx_Bx(int basis_size, double s1, double s2, double Jmin, double Jmax, int NJ, double Bmin, double Bmax, int NB);
void run_time_evolution_H0(double B1, double B2, double J1, double J2, double s1, double s2, int basis_size, int M);
void run_time_evolution_product_state(double Bz, double Bx, double J0, double s1, double s2, int basis_size, double m1, double m2, double k);
void run_time_evolution_product_state_different_atoms_coeff(double Bz, double Bx, double J0, double g1, double g2, double s1, double s2, int basis_size, double m1, double m2, int ind, int ind_end);
void run_time_evolution_product_state_different_atoms_eigen(double Bz, double Bx, double J0, double g1, double g2, double s1, double s2, int basis_size, double m1, double m2, int ind);
void run_time_evolution_product_state_different_atoms(double s1, double s2, int basis_size);
void run_basis_convergence(double s1, double s2,double J, double Bz);


/////////////////////////////////////////////////////////////////////////////////////////////////////////

//void run_time_evolution_occupation_H0(double B1, double B2, double J1, double J2, double s1, double s2, int basis_size, int M_init);
//void run_tests(double B, double J, double s1, double s2, int basis_size,double g1, double g2);
//void run_time_evolution_m1m2(double B1, double B2, double J1, double J2, double s1, double s2, int basis_size, double m1_init, double m2_init, int n_init);
//void run_occupation_B(int basis_size, double s1, double s2);
//void run_occupation_J(int basis_size, double s1, double s2);


//double exp_value_basis_Mx(State state1, State state2);
//void time_evolution_occupation(vector<gsl_vector_complex> eigenvectors_init, vector<gsl_matrix_complex> eigenvectors_fin, vector<gsl_vector> Eigenvalues_fin, double t0, double t1, double dt, double s1, int basis_size, int M_init, vector<vector<State>> basis, double B1, double B2, double J1, double J2, double m1);
//double mean_S(vector<gsl_matrix_complex> eigenvectors_fin, int ind_spin,  int ind_eigenstate, int M, vector<vector<State>> basis, double s1);
//double mean_n(vector<gsl_matrix_complex> eigenvectors_fin, int ind_spin,  int ind_eigenstate, int M, vector<vector<State>> basis, double s1);
//double exp_value_basis_m1x(State state1, State state2);
//double exp_value_basis_m2x(State state1, State state2);
//gsl_complex exp_value_X(vector<gsl_matrix_complex> eigenvectors, int ind_eigenstate1, int ind_eigenstate2, int ind_spin1, int ind_spin2, int basis_size, double s1, int M1, int M2, vector<vector<State>> basis, int flag=0);
//gsl_complex exp_value_M(vector<gsl_matrix_complex> eigenvectors, int ind_eigenstate1, int ind_eigenstate2, int ind_spin1, int ind_spin2, int basis_size, double s1, int M1, int M2);
//gsl_complex exp_value_S(vector<gsl_matrix_complex> eigenvectors, int ind_eigenstate1, int ind_eigenstate2, int ind_spin1, int ind_spin2, int basis_size, double s1, int M1, int M2);

//void ToFileTime_old(string file_name, double t, gsl_complex m1);


#endif
