#include "fun.h"
#include <gsl/gsl_sf_hermite.h>


//Kronecker's delta
int DK(double a, double b){
    if(a==b) return 1;
    else return 0;
}

// generate Hamiltonian
gsl_matrix_complex*  generate_Hamiltonian(vector<State> basis_set, double J, double Bz, double Bx, double g1, double g2){
 
    int N = basis_set.size();
    gsl_matrix_complex * H = gsl_matrix_complex_calloc(N,N);


    double s1 = basis_set[0].s1;
    double s2 = basis_set[0].s2;

    for(int i=0; i<N; i++)
        for(int j=0; j<N; j++)
            {
                double SUM_ss=0;
                double SUM_zeeman=0;
                for(double m1=-s1; m1<=s1; m1++) // sum over m1,m2,m1',m2' 
                    for(double m2=-s2; m2<=s2; m2++)
                    {   
//                        cout << "S " << basis_set[i].S << " M " << basis_set[i].M;
//                        cout << " m1 " << m1 << " m2 " << m2; 
//                        cout << " CP=" << CG_coff(s1, s2, m1, m2, basis_set[i].S, basis_set[i].M) << endl;

                        for(double m3=-s1; m3<=s1; m3++) //m3=m1^\prime
                            for(double m4=-s2; m4<=s2; m4++){ //m4=m2^\prime
                                // CG_coff - Clebsch–Gordan coef (CG.cpp)
                                SUM_ss+=CG_coff(s1, s2, m1, m2, basis_set[i].S, basis_set[i].M)*CG_coff(s1, s2, m3, m4, basis_set[j].S, basis_set[j].M)*
                                        (m1*m2*DK(m1,m3)*DK(m2,m4)+0.5*sqrt(s2*(s2+1)-m4*(m4+1))*sqrt(s1*(s1+1)-m3*(m3-1))*DK(m2,m4+1)*DK(m1,m3-1)+0.5*
                                            sqrt(s2*(s2+1)-m4*(m4-1))*sqrt(s1*(s1+1)-m3*(m3+1))*DK(m2,m4-1)*DK(m1,m3+1));
                                SUM_zeeman+=CG_coff(s1, s2, m1, m2, basis_set[i].S, basis_set[i].M)*CG_coff(s1, s2, m3, m4, basis_set[j].S, basis_set[j].M)*
                                            (Bz*(g1*m1+g2*m2)*DK(m1,m3)*DK(m2,m4) + 0.5*Bx*
                                            (
                                                g1*sqrt(s1*(s1+1)-m3*(m3+1)) *DK(m1,m3+1)*DK(m2,m4)+
                                                g1*sqrt(s1*(s1+1)-m3*(m3-1)) *DK(m1,m3-1)*DK(m2,m4)+
                                                g2*sqrt(s2*(s2+1)-m4*(m4+1)) *DK(m1,m3)*DK(m2,m4+1)+
                                                g2*sqrt(s2*(s2+1)-m4*(m4-1)) *DK(m1,m3)*DK(m2,m4-1)   ));
                    }           
                    }           
            
                                
                //cout  << "########" << endl;
                //cout << basis_set[i].n << " " << basis_set[j].n << endl;
                //cout << SUM_ss << " " << gsl_sf_hermite_phys(basis_set[i].n, 0)*gsl_sf_hermite_phys(basis_set[j].n, 0) << endl;
                // gsl_sf_hermite_phys - Hermite polynomials (GSL library)
                double psi_i, psi_j;
                if(basis_set[i].n >=170)
                    psi_i = 0;
                else
                    psi_i = 1/sqrt(pow(2,basis_set[i].n)*gsl_sf_fact(basis_set[i].n))*pow(1/M_PI,0.25)*gsl_sf_hermite_phys(basis_set[i].n, 0);
                if(basis_set[j].n >=170)
                    psi_j = 0;
                else
                    psi_j = 1/sqrt(pow(2,basis_set[j].n)*gsl_sf_fact(basis_set[j].n))*pow(1/M_PI,0.25)*gsl_sf_hermite_phys(basis_set[j].n, 0);
                double ME_ss = SUM_ss*2/sqrt(2)*J*psi_i*psi_j;
                double ME_zeeman = SUM_zeeman*uB*DK(basis_set[i].n,basis_set[j].n);

                //harmonic oscillator energy
                double diag;
                if(i==j)
                    diag = (basis_set[i].n+0.5);
                else
                    diag = 0;
                
                gsl_complex h = gsl_complex_rect( (ME_ss + ME_zeeman + diag), 0 ); 
                gsl_matrix_complex_set(H,i,j,h); 
  

            }


    return H;

}


// print Hamiltonian
void print_ham(gsl_matrix_complex* H, vector<State> basis_set){
    cout << "Hamiltonian" << endl;
    int N = basis_set.size();
    for(int i=0; i<N; i++){
    for(int j=0; j<N; j++)
        cout << round(100000*GSL_REAL(gsl_matrix_complex_get(H,i,j)))/100000.0 << "+" << round(100000*GSL_IMAG(gsl_matrix_complex_get(H,i,j)))/10000.0 << "i ";
    cout << endl;
    }
}

//print eigenstates
void print_eigen(gsl_matrix_complex* eigenvector, gsl_vector* eigenvalue, vector<State> basis_set,int basis_size){
    cout << "Eigenstates" << endl;

    int N = basis_set.size();
    cout << "S,M,n:" << endl;
    for(int j=0; j<N; j++) 
        cout << basis_set[j].S << "," << basis_set[j].M << "," << basis_set[j].n << "  ";
    cout << endl;
    for(int i=0; i<N; i++){ //states
    for(int j=0; j<N; j++){ // coeff in each state
        cout << round(10000*gsl_complex_abs2(gsl_matrix_complex_get(eigenvector,j,i)))/10000.0;
        if(j%basis_size==basis_size-1) 
            cout << " |";
        else
            cout << " ";
    }
    cout << " E=" << round(100*gsl_vector_get(eigenvalue,i))/100.0 << endl;
    }
}




// diagonalization of Hamilotnian matrix
pair<gsl_vector*,gsl_matrix_complex*> diagonalize_Hamiltonian(gsl_matrix_complex* H, int basis_size,vector<State> basis){

 
    gsl_eigen_hermv_workspace* workspace; //workspace required by GSL diagonalization
    workspace = gsl_eigen_hermv_alloc(basis_size);
    gsl_vector* eigenvalues = gsl_vector_alloc(basis_size);
    gsl_matrix_complex* eigenvectors = gsl_matrix_complex_calloc(basis_size,basis_size);
    
   // cout << "Diagonalization" << endl;
  //  print_ham(H,basis);
    gsl_eigen_hermv(H, eigenvalues, eigenvectors, workspace); // diagonalization
    gsl_eigen_hermv_sort(eigenvalues, eigenvectors, GSL_EIGEN_SORT_VAL_ASC); //sorting of eigenvalues and eigenvectors

    gsl_vector* StateNumber = gsl_vector_alloc(basis_size);
    gsl_vector* StateNumber2 = gsl_vector_alloc(basis_size);


    
    int M=abs(basis[0].M);
    double s1=basis[0].s1;
    int N=(2*s1-abs(M)+1);
    //cout << "M=" << M << "size:" << eigenvalues->size << " " << basis_size << endl;
    cout << "Ham diagonalized, start sorting" << endl;
    for(uint l =0; l<eigenvalues->size; l++){
        double S[200]={0}; //table of size equal to number of spin components
        double MM[200]={0}; //table of size equal to number of spin components

        double meanval_n=0;
        double meanval_S=0;
        double meanval_M=0;
        int maxM=0;
        int maxn=0;
        //index would be S-abs(M) from lowest S
        double max_ev=0;
        //int max_en=0;
       
        double check_sum=0;
        for(uint j =0; j<eigenvalues->size; j++){
            check_sum+=gsl_complex_abs2(gsl_matrix_complex_get(eigenvectors,j,l));
            if(gsl_complex_abs2(gsl_matrix_complex_get(eigenvectors,j,l))>max_ev){
                maxM = basis[j].M;
                maxn= basis[j].n;
               max_ev = gsl_complex_abs2(gsl_matrix_complex_get(eigenvectors,j,l));
            }
            //cout << basis[j].S << endl; 
            meanval_n += gsl_complex_abs2(gsl_matrix_complex_get(eigenvectors,j,l))*basis[j].n;
            meanval_S += gsl_complex_abs2(gsl_matrix_complex_get(eigenvectors,j,l))*basis[j].S;
            meanval_M += gsl_complex_abs2(gsl_matrix_complex_get(eigenvectors,j,l))*basis[j].M;
            S[int(basis[j].S)]+=gsl_complex_abs2(gsl_matrix_complex_get(eigenvectors,j,l)); //sum up all harmonic components to see which S is dominant
            MM[int(basis[j].M+30)]+=gsl_complex_abs2(gsl_matrix_complex_get(eigenvectors,j,l)); //sum up all harmonic components to see which S is dominant

        }
    
        int S_max=  ( max_element(S, S + 200) - S ) ;
        int M_max=  ( max_element(MM, MM + 200) - MM ) - 30 ;
        //cout << S_max << " " << M << endl;
        //cout << check_sum << endl;
        if( (check_sum-1)>0.01 ) cout << "State is not normalized!" << endl;

        //set the value which you want to use to sort eigenstates
        gsl_vector_set(StateNumber,l,maxn*10000+(meanval_M+2*s1)*100+gsl_vector_get(eigenvalues,l)*1);
      
        }
    
    gsl_vector_memcpy(StateNumber2, StateNumber);
  
    gsl_eigen_hermv_sort(StateNumber, eigenvectors, GSL_EIGEN_SORT_VAL_ASC);
    gsl_sort_vector2(StateNumber2, eigenvalues);


    gsl_eigen_hermv_free(workspace);
    gsl_vector_free(StateNumber);
    gsl_vector_free(StateNumber2);

        double meanval_n=0;
        double meanval_S=0;
        double meanval_M=0;
    for(uint j =0; j<eigenvalues->size; j++){
          
            meanval_n += gsl_complex_abs2(gsl_matrix_complex_get(eigenvectors,j,0))*basis[j].n;
            meanval_S += gsl_complex_abs2(gsl_matrix_complex_get(eigenvectors,j,0))*basis[j].S;
            meanval_M += gsl_complex_abs2(gsl_matrix_complex_get(eigenvectors,j,0))*basis[j].M;
           
    }

    return make_pair(eigenvalues,eigenvectors);


}


//calculate expectation value of Hamiltonian
gsl_vector* expectation_value(gsl_matrix_complex* eigenvectors_t,vector<State> basis_set, double J, double Bz){


    int N = basis_set.size();


    double s1 = basis_set[0].s1;
    double s2 = basis_set[0].s2;

    gsl_vector* expec_values = gsl_vector_alloc(N);

    for(int n =0; n<N; n++){ //loop over eigenstates
        double En=0;
    for(int i=0; i<N; i++) //loop over elements of mixed eigenstate
            {
                double j=i;

                double cj2 = gsl_complex_abs2(gsl_matrix_complex_get(eigenvectors_t,j,n)); //coefficient



                double SUM_ss=0;
                double SUM_zeeman=0;
                for(double m1=-s1; m1<=s1; m1++) // sum over m1,m2,m1',m2' 
                    for(double m2=-s2; m2<=s2; m2++)
                    {   
                        for(double m3=-s1; m3<=s1; m3++) //m3=m1^\prime
                            for(double m4=-s2; m4<=s2; m4++){ //m4=m2^\prime
                                // CG_coff - Clebsch–Gordan coef (CG.cpp)
                                SUM_ss+=CG_coff(s1, s2, m1, m2, basis_set[i].S, basis_set[i].M)*CG_coff(s1, s2, m3, m4, basis_set[j].S, basis_set[j].M)*
                                        (m1*m2*DK(m1,m3)*DK(m2,m4)+0.5*sqrt(s2*(s2+1)-m4*(m4+1))*sqrt(s1*(s1+1)-m3*(m3-1))*DK(m2,m4+1)*DK(m1,m3-1)+0.5*
                                            sqrt(s2*(s2+1)-m4*(m4-1))*sqrt(s1*(s1+1)-m3*(m3+1))*DK(m2,m4-1)*DK(m1,m3+1));
                                SUM_zeeman+=CG_coff(s1, s2, m1, m2, basis_set[i].S, basis_set[i].M)*CG_coff(s1, s2, m3, m4, basis_set[j].S, basis_set[j].M)*
                                            (m1+m2)*DK(m1,m3)*DK(m2,m4);
                    }           
            
                                }
                // gsl_sf_hermite_phys - Hermite polynomials (GSL library)
                double psi_i = 1/sqrt(pow(2,basis_set[i].n)*gsl_sf_fact(basis_set[i].n))*pow(1/M_PI,0.25)*gsl_sf_hermite_phys(basis_set[i].n, 0);
                double psi_j = 1/sqrt(pow(2,basis_set[j].n)*gsl_sf_fact(basis_set[j].n))*pow(1/M_PI,0.25)*gsl_sf_hermite_phys(basis_set[j].n, 0);
                double ME_ss = SUM_ss*2/sqrt(2)*J*psi_i*psi_j;
                double ME_zeeman = SUM_zeeman*2*uB*Bz*DK(basis_set[i].n,basis_set[j].n);

                //harmonic oscillator energy
                double diag = (basis_set[i].n+0.5);
         
                
                En+=cj2*(ME_ss + ME_zeeman + diag); 
  

            }
        gsl_vector_set(expec_values,n,En);
    }
    

    return expec_values;
}



// calculate energy for the basis states
gsl_vector* basis_energy(vector<State> basis_set, double J, double Bz){


    int N = basis_set.size();


    double s1 = basis_set[0].s1;
    double s2 = basis_set[0].s2;

    gsl_vector* En = gsl_vector_alloc(N);


    for(int i=0; i<N; i++) //loop over elements of basis states
            {
                double j=i;


                double SUM_ss=0;
                double SUM_zeeman=0;
                for(double m1=-s1; m1<=s1; m1++) // sum over m1,m2,m1',m2' 
                    for(double m2=-s2; m2<=s2; m2++)
                    {   
                        for(double m3=-s1; m3<=s1; m3++) //m3=m1^\prime
                            for(double m4=-s2; m4<=s2; m4++){ //m4=m2^\prime
                                // CG_coff - Clebsch–Gordan coef (CG.cpp)
                                SUM_ss+=CG_coff(s1, s2, m1, m2, basis_set[i].S, basis_set[i].M)*CG_coff(s1, s2, m3, m4, basis_set[j].S, basis_set[j].M)*
                                        (m1*m2*DK(m1,m3)*DK(m2,m4)+0.5*sqrt(s2*(s2+1)-m4*(m4+1))*sqrt(s1*(s1+1)-m3*(m3-1))*DK(m2,m4+1)*DK(m1,m3-1)+0.5*
                                            sqrt(s2*(s2+1)-m4*(m4-1))*sqrt(s1*(s1+1)-m3*(m3+1))*DK(m2,m4-1)*DK(m1,m3+1));
                                SUM_zeeman+=CG_coff(s1, s2, m1, m2, basis_set[i].S, basis_set[i].M)*CG_coff(s1, s2, m3, m4, basis_set[j].S, basis_set[j].M)*
                                            (m1+m2)*DK(m1,m3)*DK(m2,m4);
                    }           
            
                                }
                // gsl_sf_hermite_phys - Hermite polynomials (GSL library)
                double psi_i = 1/sqrt(pow(2,basis_set[i].n)*gsl_sf_fact(basis_set[i].n))*pow(1/M_PI,0.25)*gsl_sf_hermite_phys(basis_set[i].n, 0);
                double psi_j = 1/sqrt(pow(2,basis_set[j].n)*gsl_sf_fact(basis_set[j].n))*pow(1/M_PI,0.25)*gsl_sf_hermite_phys(basis_set[j].n, 0);
                double ME_ss = SUM_ss*2/sqrt(2)*J*psi_i*psi_j;
                double ME_zeeman = SUM_zeeman*2*uB*Bz*DK(basis_set[i].n,basis_set[j].n);

                //harmonic oscillator energy
                double diag = (basis_set[i].n+0.5);
         
                
                gsl_vector_set(En,i,(ME_ss + ME_zeeman + diag));  
  

            }
    

    return En;
}


// calculate g-factor based on the quantum numbers of atom
double g(double S, double J, double L, double I, double F, double mu){

    double gL=1;
    double gS=2;
    double gI;
    if(I==0 || mu==0)
        gI=0;
    else
        gI=I/mu;

    double SS=S*(S+1);
    double JJ=J*(J+1);
    double LL=L*(L+1);
    double II=I*(I+1);
    double FF=F*(F+1);

    double gJ=gL*(JJ-SS+LL)/(2*JJ) + gS*(JJ+SS-LL)/(2*JJ);
    double gF=gJ*(FF-II+JJ)/(2*FF) + uB/uN*gI*(FF+II-JJ)/(2*FF);

    return gF;
}
