#include "fun.h"


// constuctor of class State
State::State(int cn, double cs1, double cs2, double cS, double cM){ 
    n=cn;
    s1=cs1;
    s2=cs2;
    S=cS;
    M=cM;
}

//method printing quantum numbers
void State::PrintQuantumNumbers(){ 
    cout << "n=" << n;
    cout << " s1=" << s1;
    cout << " s2=" << s2;
    cout << " S=" << S;
    cout << " M=" << M;
    cout << endl;
}

// function generating basis states
vector<State> generate_basis(double s1, double s2, int basis_size, bool S_conserved, bool M_conserved, bool symmetry, int M, int S){
    if(symmetry==0){
        if(S_conserved && M_conserved)
            return generate_nosymmetry_basis_SM(s1, s2, basis_size,S,M);
        else if(M_conserved)
            return generate_nosymmetry_basis_M(s1, s2, basis_size,M);
        else if(S_conserved)
            return generate_nosymmetry_basis_S(s1, s2, basis_size,S);
        else
            return generate_nosymmetry_basis(s1, s2, basis_size);
    }
    else if( fmod(s1,1)==0.5 && fmod(s2,1)==0.5){ //determine if system is fermionic
        if(S_conserved && M_conserved)
            return generate_fermion_basis_SM(s1, s2, basis_size,S,M);
        else if(M_conserved)
            return generate_fermion_basis_M(s1, s2, basis_size,M);
        else if(S_conserved)
            return generate_fermion_basis_S(s1, s2, basis_size,S);
        else
            return generate_fermion_basis(s1, s2, basis_size);
        }
    else if( fmod(s1,1)==0 && fmod(s2,1)==0){ //determine if system is bosonic
        if(S_conserved && M_conserved)
            return generate_boson_basis_SM(s1, s2, basis_size,S,M);
        else if(M_conserved)
            return generate_boson_basis_M(s1, s2, basis_size,M);
        else if(S_conserved)
            return generate_boson_basis_S(s1, s2, basis_size,S);
        else
            return generate_boson_basis(s1, s2, basis_size);
        }
    else //else system is mix of fermion and boson (not considered situation)
        {
        cout << "Boson and fermion system" << endl;
        vector<State> null;
        return null;
        }
}

// generating fermion basis states with specific S,M and saving basis to file
vector<State> generate_fermion_basis_SM(double s1, double s2, int basis_size,int S, int M){
    vector<State> basis_states;
    
    ofstream file;
    string file_name = "Basis_s12_"+to_string(s1)+".dat";
    file.open (file_name, std::ofstream::out);


        int n_start;
        if(fmod(S,2) == fmod(s1+s2,2)) // if condition is true then |SM> is symmetric, then we consider odd n to obtain antisymmetric final state
            n_start = 1;
        else                         // else |SM> is antisymmetric, then we consider even n to obtain antisymmetric final state
            n_start = 0;
        for(int n=n_start; n<basis_size*2; n+=2)
            {
            State base_state(n,s1,s2,S,M);
            basis_states.push_back(base_state);
            file << "n=" << n << " S=" << S << " M=" << M << endl;
            }
        
    file.close();
    return basis_states;
}

// generating fermonic basis states with specific M and saving basis to file
vector<State> generate_fermion_basis_M(double s1, double s2, int basis_size, int M){
    vector<State> basis_states;
    
    ofstream file;
    string file_name = "Basis_s12_"+to_string(s1)+".dat";
    file.open (file_name, std::ofstream::out);

    for(int S=s1+s2; S>=abs(M); S-=1){ //start from symmetric state
        int n_start;
        if(fmod(S,2) == fmod(s1+s2,2)) // if condition is true then |SM> is symmetric, then we consider odd n to obtain antisymmetric final state
            n_start = 1;
        else                         // else |SM> is antisymmetric, then we consider even n to obtain antisymmetric final state
            n_start = 0;
        for(int n=n_start; n<basis_size*2; n+=2)
            {
            State base_state(n,s1,s2,S,M);
            basis_states.push_back(base_state);
            file << "n=" << n << " S=" << S << " M=" << M << endl;
            }
        
    }
        
    file.close();
    return basis_states;
}

// generating fermonic basis states with specific S and saving basis to file
vector<State> generate_fermion_basis_S(double s1, double s2, int basis_size, int S){
    vector<State> basis_states;
    
    ofstream file;
    string file_name = "Basis_s12_"+to_string(s1)+".dat";
    file.open (file_name, std::ofstream::out);

    for(int M=S; M>=-S; M-=1){ //start from symmetric state
        int n_start;
        if(fmod(S,2) == fmod(s1+s2,2)) // if condition is true then |SM> is symmetric, then we consider odd n to obtain antisymmetric final state
            n_start = 1;
        else                         // else |SM> is antisymmetric, then we consider even n to obtain antisymmetric final state
            n_start = 0;
        for(int n=n_start; n<basis_size*2; n+=2)
            {
            State base_state(n,s1,s2,S,M);
            basis_states.push_back(base_state);
            file << "n=" << n << " S=" << S << " M=" << M << endl;
            }
        
    }
        
    file.close();
    return basis_states;
}



// generating fermonic basis states and saving basis to file
vector<State> generate_fermion_basis(double s1, double s2, int basis_size){
    vector<State> basis_states;
    
    ofstream file;
    string file_name = "Basis_s12_"+to_string(s1)+".dat";
    file.open (file_name, std::ofstream::out);

    for(int S=s1+s2; S>=0; S-=1){ //start from symmetric state
        for(int M=S; M>=-S; M-=1){
        int n_start;
        if(fmod(S,2) == fmod(s1+s2,2)) // if condition is true then |SM> is symmetric, then we consider odd n to obtain antisymmetric final state
            n_start = 1;
        else                         // else |SM> is antisymmetric, then we consider even n to obtain antisymmetric final state
            n_start = 0;
        for(int n=n_start; n<basis_size*2; n+=2)
            {
            State base_state(n,s1,s2,S,M);
            basis_states.push_back(base_state);
            file << "n=" << n << " S=" << S << " M=" << M << endl;
            }
        }
    }
        
    file.close();
    return basis_states;
}

// generating boson basis states with specific S,M and saving basis to file
vector<State> generate_boson_basis_SM(double s1, double s2, int basis_size,int S, int M){
    vector<State> basis_states;
    
    ofstream file;
    string file_name = "Basis_s12_"+to_string(s1)+".dat";
    file.open (file_name, std::ofstream::out);

        int n_start;
        if(fmod(S,2) == fmod(s1+s2,2)) // if condition is true then |SM> is symmetric, then we consider odd n to obtain antisymmetric final state
            n_start = 0;
        else                          // else |SM> is antisymmetric, then we consider even n to obtain antisymmetric final state
            n_start = 1;
        for(int n=n_start; n<basis_size*2; n+=2)
                {
                State base_state(n,s1,s2,S,M);
                basis_states.push_back(base_state);
                file << "n=" << n << " S=" << S << " M=" << M << endl;
                }
        
    file.close();
    return basis_states;
}

// generating boson basis states with specific S and saving basis to file
vector<State> generate_boson_basis_S(double s1, double s2, int basis_size, int S){
    vector<State> basis_states;
    ofstream file;
    string file_name = "Basis_s12_"+to_string(s1)+".dat";
    file.open (file_name, std::ofstream::out);
    for(int M=S; M>=-S; M-=1){ 
        int n_start;
        if(fmod(S,2) == fmod(s1+s2,2)) // if condition is true then |SM> is symmetric, then we consider odd n to obtain antisymmetric final state
            n_start = 0;
        else                          // else |SM> is antisymmetric, then we consider even n to obtain antisymmetric final state
            n_start = 1;
        for(int n=n_start; n<basis_size*2; n+=2)
                {
                State base_state(n,s1,s2,S,M);
                basis_states.push_back(base_state);
                file << "n=" << n << " S=" << S << " M=" << M << endl;
                }
    }
    file.close();
    return basis_states;
}


// generating boson basis states with specfic M and saving basis to file
vector<State> generate_boson_basis_M(double s1, double s2, int basis_size, int M){
    vector<State> basis_states;
    ofstream file;
    string file_name = "Basis_s12_"+to_string(s1)+".dat";
    file.open (file_name, std::ofstream::out);
    for(int S=s1+s2; S>=abs(M); S-=1){ 
        int n_start;
        if(fmod(S,2) == fmod(s1+s2,2)) // if condition is true then |SM> is symmetric, then we consider odd n to obtain antisymmetric final state
            n_start = 0;
        else                          // else |SM> is antisymmetric, then we consider even n to obtain antisymmetric final state
            n_start = 1;
        for(int n=n_start; n<basis_size*2; n+=2)
                {
                State base_state(n,s1,s2,S,M);
                basis_states.push_back(base_state);
                file << "n=" << n << " S=" << S << " M=" << M << endl;
                }
    }
    file.close();
    return basis_states;
}

// generating boson basis states and saving basis to file
vector<State> generate_boson_basis(double s1, double s2, int basis_size){
    vector<State> basis_states;
    
    ofstream file;
    string file_name = "Basis_s12_"+to_string(s1)+".dat";
    file.open (file_name, std::ofstream::out);

    for(int S=s1+s2; S>=0; S-=1){ //start from symmetric state
        for(int M=S; M>=-S; M-=1){
        int n_start;
        if(fmod(S,2) == fmod(s1+s2,2)) // if condition is true then |SM> is symmetric, then we consider odd n to obtain antisymmetric final state
            n_start = 0;
        else                          // else |SM> is antisymmetric, then we consider even n to obtain antisymmetric final state
            n_start = 1;
        for(int n=n_start; n<basis_size*2; n+=2)
                {
                State base_state(n,s1,s2,S,M);
                basis_states.push_back(base_state);
                file << "n=" << n << " S=" << S << " M=" << M << endl;
                }
        }
    }
    file.close();
    return basis_states;
}


// generating nosymmetry basis states with specific S,M and saving basis to file
vector<State> generate_nosymmetry_basis_SM(double s1, double s2, int basis_size,int S, int M){
    vector<State> basis_states;
    
    ofstream file;
    string file_name = "Basis_s12_"+to_string(s1)+".dat";
    file.open (file_name, std::ofstream::out);

        int n_start;
        n_start = 0;
        for(int n=n_start; n<basis_size; n+=1)
                {
                State base_state(n,s1,s2,S,M);
                basis_states.push_back(base_state);
                file << "n=" << n << " S=" << S << " M=" << M << endl;
                }
        
    file.close();
    return basis_states;
}

// generating nosymmetry basis states with specific S and saving basis to file
vector<State> generate_nosymmetry_basis_S(double s1, double s2, int basis_size, int S){
    vector<State> basis_states;
    
    ofstream file;
    string file_name = "Basis_s12_"+to_string(s1)+".dat";
    file.open (file_name, std::ofstream::out);

    for(int M=S; M>=-S; M-=1){ //start from symmetric state
        int n_start;
        n_start = 0;

        for(int n=n_start; n<basis_size; n+=1)
            {
            State base_state(n,s1,s2,S,M);
            basis_states.push_back(base_state);
            file << "n=" << n << " S=" << S << " M=" << M << endl;
            }
    }
    file.close();
    return basis_states;
}

// generating nosymmetry basis states with specific M and saving basis to file
vector<State> generate_nosymmetry_basis_M(double s1, double s2, int basis_size, int M){
    vector<State> basis_states;
    
    ofstream file;
    string file_name = "Basis_s12_"+to_string(s1)+".dat";
    file.open (file_name, std::ofstream::out);

    for(int S=s1+s2; S>=abs(M); S-=1){ //start from symmetric state
        int n_start;
        n_start = 0;

        for(int n=n_start; n<basis_size; n+=1)
            {
            State base_state(n,s1,s2,S,M);
            basis_states.push_back(base_state);
            file << "n=" << n << " S=" << S << " M=" << M << endl;
            }
    }
    file.close();
    return basis_states;
}


// generating nosymmetry basis states and saving basis to file
vector<State> generate_nosymmetry_basis(double s1, double s2, int basis_size){
    vector<State> basis_states;
    
    ofstream file;
    string file_name = "Basis_s12_"+to_string(s1)+".dat";
    file.open (file_name, std::ofstream::out);

    for(double S=s1+s2; S>=0; S-=1){ //start from symmetric state
        for(double M=S; M>=-S; M-=1){
        int n_start;
        n_start = 0;
       
        for(int n=n_start; n<basis_size; n+=1)
                {
                State base_state(n,s1,s2,S,M);
                basis_states.push_back(base_state);
                file << "n=" << n << " S=" << S << " M=" << M << endl;
                }
        }
    }
    file.close();
    return basis_states;
}