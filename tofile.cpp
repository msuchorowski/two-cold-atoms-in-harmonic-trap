#include "fun.h"


//function saving eigenvectors to file for conserved S,M
void ToFile_SM(string file_name,  gsl_vector* Eigenvalues, gsl_matrix_complex* eigenvectors, double B, double J, int S, int M){

    ofstream file;
    file.open (file_name, std::ofstream::out|std::ofstream::app);


    file << J << " " << B << " " << S << " " << M << " ";
    for(uint i=0; i<Eigenvalues->size; i++){
        file << gsl_vector_get(Eigenvalues,i) << " ";
        }
        file << endl;

    file.close();

}


//function saving eigenvectors to file for conserved M
void ToFile_M(string file_name,  gsl_vector* Eigenvalues, gsl_matrix_complex* eigenvectors, double B, double J, int M, vector<State> basis, double s1){

    ofstream file;
    file.open (file_name, std::ofstream::out|std::ofstream::app);

    // states should be ordered by S
    int S_prev=99;
    //cout << "To file" << endl;
   // cout << B << "  " << J << " " << M << endl;
    int N=(2*s1-abs(M)+1);

    for(uint l =0; l<Eigenvalues->size; l++){
        double S[N]={0}; //table of size equal to number of spin components
        // index would be abs(M)-S+1
        for(uint j =0; j<Eigenvalues->size; j++){
            S[int(basis[j].S-abs(M))]+=gsl_complex_abs2(gsl_matrix_complex_get(eigenvectors,j,l)); //sum up all harmonic components to see which S is dominant
        }
        int S_max= M + ( max_element(S, S + N )  - S ) ;
        if(S_prev!=S_max){
            file << endl;
            file << J << " " << B << " " << M << " " << S_max << " ";
            file << gsl_vector_get(Eigenvalues,l) << " ";
        }
        else
            file << gsl_vector_get(Eigenvalues,l) << " ";
        S_prev=S_max;


    }
}


//function saving eigenvectors to file for conserved S
void ToFile_S(string file_name,  gsl_vector* Eigenvalues, gsl_matrix_complex* eigenvectors, double B, double J, int S, vector<State> basis, double s1){

    string file_name_meanM = file_name+"_MEAN_M";
    ofstream file;
    file.open (file_name, std::ofstream::out|std::ofstream::app);
    ofstream fileM;
    fileM.open (file_name_meanM, std::ofstream::out|std::ofstream::app);

    // states should be ordered by M
   // int M_prev=99;
    //cout << "To file" << endl;
   // cout << B << "  " << J << " " << M << endl;
  //  int N=2*S+1;
    file << J << " " << B << " " << " " << S << " ";
    fileM << J << " " << B << " " << " " << S << " ";


    for(uint l =0; l<Eigenvalues->size; l++){
        double meanM=0;
        for(uint k = 0; k<Eigenvalues->size; k++)
            meanM+=gsl_complex_abs2(gsl_matrix_complex_get(eigenvectors,k,l))*basis[k].M;
        
        gsl_complex meanMx=gsl_complex_rect(0,0);
        for(int i=0; i<Eigenvalues->size; i++)
            for(int j=0; j<Eigenvalues->size; j++)
                if(basis[i].n==basis[j].n)
                {
                 double M=basis[i].M;
                 double Mp=basis[j].M;               
                 gsl_complex aa=gsl_complex_mul(gsl_matrix_complex_get(eigenvectors,i,l) , gsl_matrix_complex_get(eigenvectors,j,l)  );  
                 if(M==(Mp+1)) meanMx=gsl_complex_add(meanMx,gsl_complex_mul_real(aa,0.5*sqrt(S*(S+1)-Mp*(Mp+1))));
                 else if(M==(Mp-1)) meanMx=gsl_complex_add(meanMx,gsl_complex_mul_real(aa,0.5*sqrt(S*(S+1)-Mp*(Mp-1))));  
                }
        
        file << gsl_vector_get(Eigenvalues,l) << " ";
        fileM << GSL_REAL(meanMx) << " ";
    }
    file << endl;
    fileM << endl;


   
  //  file << J << " " << B << " " << M << " ";
  //  for(uint i=0; i<Eigenvalues->size; i++){
  //      file << gsl_vector_get(Eigenvalues,i) << " ";
  //      }
  //      file << endl;

    file.close();
    fileM.close();


}

//function saving eigenvectors to file
void ToFile(string file_name,  gsl_vector* Eigenvalues, gsl_matrix_complex* eigenvectors, double B, double J){

    ofstream file;
    file.open (file_name, std::ofstream::out|std::ofstream::app);


    file << J << " " << B << " ";
    for(uint i=0; i<Eigenvalues->size; i++){
        file << gsl_vector_get(Eigenvalues,i) << " ";
        }
        file << endl;

    file.close();

}


//function saving phase diagram to file 
void ToFilePhase(string file_name, double B, double J, double S, double M, double domM){

    ofstream file;
    file.open (file_name, std::ofstream::out|std::ofstream::app);


    file << J << " " << B << " " << S << " " << M << " " << domM << endl;

    file.close();

}

//function saving quantum numbers of the state to file assuming that state is a pure state
void StateToFile_Bx(string file_name, double B, double J, double s, gsl_vector* Eigenvalues, gsl_matrix_complex* eigenvectors,vector <State> basis){

    ofstream file;
    file.open (file_name, std::ofstream::out|std::ofstream::app);


    file << "J=" << J << " B=" << B << " ";
 
    for(uint l =0; l<Eigenvalues->size; l++){
        
        double Tab_M[13]={0};
        //cout << "EIGENVALUE E_" << l << endl;
        double max_ev=0;
        int max_en=0;
        double mean_M=0;
        double mean_M_abs=0;
       // cout << "State " << l << endl;
        for(uint j =0; j<Eigenvalues->size; j++){
            if(gsl_complex_abs2(gsl_matrix_complex_get(eigenvectors,j,l))>max_ev){
                max_en = j;
                max_ev = gsl_complex_abs2(gsl_matrix_complex_get(eigenvectors,j,l));
            }
            
            mean_M+=gsl_complex_abs2(gsl_matrix_complex_get(eigenvectors,j,l))*basis[j].M;
            mean_M_abs+=gsl_complex_abs2(gsl_matrix_complex_get(eigenvectors,j,l))*abs(basis[j].M);
            Tab_M[int(basis[j].M+6)]+= gsl_complex_abs2(gsl_matrix_complex_get(eigenvectors,j,l)) ;
            }

        
       // for(int i=0; i<13; i++)
       //     cout << i-6 << " " << Tab_M[i] << endl;

        file << J << " " << B << " " << basis[max_en].n << " " << basis[max_en].S << " " << basis[max_en].M << " " << mean_M << " " << mean_M_abs <<  endl;
          
        }



    file.close();
}


//function saving quantum numbers of the state to file assuming that state is a pure state
void StateToFile(string file_name, double B, double J, double s, gsl_vector* Eigenvalues, gsl_matrix_complex* eigenvectors,vector <State> basis){

    ofstream file;
    file.open (file_name, std::ofstream::out|std::ofstream::app);


    file << "J=" << J << " B=" << B << " ";
 
    for(uint l =0; l<Eigenvalues->size; l++){
        
        
        //cout << "EIGENVALUE E_" << l << endl;
        double max_ev=0;
        int max_en=0;
        for(uint j =0; j<Eigenvalues->size; j++){
            if(gsl_complex_abs2(gsl_matrix_complex_get(eigenvectors,j,l))>max_ev){
                max_en = j;
                max_ev = gsl_complex_abs2(gsl_matrix_complex_get(eigenvectors,j,l));
            }
            //if(abs(gsl_matrix_get(eigenvectors,j,l)) > 0.0001){
            //    basis[j].PrintQuantumNumbers();
            //    cout << abs(gsl_matrix_get(eigenvectors,j,l)) << endl;
            //    }
            
            }
        
        file << "n=" << basis[max_en].n << " S=" << basis[max_en].S << " M=" << basis[max_en].M << endl;
            

        }



    file.close();

}


//function saving occupation of ground state and first excited state to file
void ToFileOccupation(string file_name, gsl_vector* Eigenvalues, gsl_matrix_complex* eigenvectors, vector <State> basis, double Bz, double J, double M, double s1){
     
    ofstream file;
    file.open (file_name, std::ofstream::out|std::ofstream::app);
  
    // states should be ordered by S
    int S_prev=99;

    for(uint l =0; l<Eigenvalues->size; l++){
        double max_ev=0;
        int max_en=0;
        //double exp_S=0;
        //double exp_M=0;

        double exp_n=0;
        for(uint j =0; j<Eigenvalues->size; j++){
            if(gsl_complex_abs2(gsl_matrix_complex_get(eigenvectors,j,l))>max_ev){
               max_en = j;
               max_ev = gsl_complex_abs2(gsl_matrix_complex_get(eigenvectors,j,l));
            }
            double n=gsl_complex_abs2(gsl_matrix_complex_get(eigenvectors,j,l))*double(basis[j].n);
            exp_n+=n;
         
        }
        
        int M_ev=basis[max_en].M;
        int S=basis[max_en].S;
        if(M!=M_ev) cout << "M number is not correct!" << endl;
        if(S_prev!=S){
            if(S_prev!=99 || M!=s1*2) file << endl;
            file << J << " " << Bz << " " << M << " " << S << " ";
            file << exp_n << " ";
        }
        else
            file << exp_n << " ";
        S_prev=S;
        
    }
    

   

    file.close();

}




//function saving eigenvectors to file for time evolution 
void ToFileTime_old(string file_name, double t, gsl_complex m1){

    ofstream file;
    file.open (file_name, std::ofstream::out|std::ofstream::app);


    file << t << " " << GSL_REAL(m1) << " " << GSL_IMAG(m1) << " " << gsl_complex_abs(m1) << endl;


    file.close();

}

//function saving eigenvectors to file for time evolution 
void ToFileTime(string file_name, double t, gsl_complex m1, gsl_complex m2, gsl_complex M){

    ofstream file;
    file.open (file_name, std::ofstream::out|std::ofstream::app);


    file << t << " " << GSL_REAL(m1) << " " << GSL_IMAG(m1) << " " << gsl_complex_abs(m1) << " " << GSL_REAL(m2) << " " << GSL_IMAG(m2) << " " << gsl_complex_abs(m2)<< " " << GSL_REAL(M) << " " << GSL_IMAG(M) << " " << gsl_complex_abs(M)<<  endl;


    file.close();

}


//function saving groundstate energy and basis size to file
void ToFileBasisSize(string file_name, double basis_size, double E){

    ofstream file;
    file.open (file_name, std::ofstream::out|std::ofstream::app);

    file << basis_size << " " << E << endl;

    file.close();

}

// save the eigenstate and eigenvalues to file
void ToFileEigenstate(gsl_matrix_complex* Eigenvectors, gsl_vector* Eigenvalues, string file_name, string file_name2){
    ofstream file;
    file.open (file_name, std::ofstream::out|std::ofstream::app);

    for(uint j =0; j<Eigenvalues->size; j++)
        for(uint i =0; i<Eigenvalues->size; i++){    
            file << GSL_REAL(gsl_matrix_complex_get(Eigenvectors,j,i)) << " " << GSL_IMAG(gsl_matrix_complex_get(Eigenvectors,j,i)) << endl;                
            cout << GSL_REAL(gsl_matrix_complex_get(Eigenvectors,j,i)) << " " << GSL_IMAG(gsl_matrix_complex_get(Eigenvectors,j,i)) << endl; 
	}

    file.close();

    ofstream file2;
    file2.open (file_name2, std::ofstream::out|std::ofstream::app);

    for(uint j =0; j<Eigenvalues->size; j++){
            file2 << gsl_vector_get(Eigenvalues,j) << endl;
            cout << gsl_vector_get(Eigenvalues,j) << endl;
         }
        
    file2.close();

}

//read eigenstates and eigenvalues from file
pair<gsl_vector*,gsl_matrix_complex*> FromFileEigenstate(string file_name, string file_name2, int basis_size){

    gsl_matrix_complex* M = gsl_matrix_complex_alloc(basis_size, basis_size);
    double A[basis_size*basis_size]={0};
    double B[basis_size*basis_size]={0};

    ifstream infile(file_name);
    std::string line;
    int counter=0;
    while (std::getline(infile, line))
    {
    std::istringstream iss(line);
    double a, b;
    if (!(iss >> a >> b)) { break; } // error
    A[counter]=a;
    B[counter]=b;
    counter++;
    }    


    for(uint j =0; j<basis_size; j++)
        for(uint i =0; i<basis_size; i++){
            int index=i+j*basis_size;
            gsl_complex c = gsl_complex_rect(A[index],B[index]);    
            gsl_matrix_complex_set(M,j,i,c);
                     
        }



    gsl_vector* V = gsl_vector_alloc(basis_size);
    ifstream infile2(file_name2);

    std::string line2;
    int counter2=0;
    while (std::getline(infile2, line2))
    {
    std::istringstream iss2(line2);
    double a2;
    if (!(iss2 >> a2)) { break; } // error
    gsl_vector_set(V,counter2,a2);
    counter2++;
    }  

    return make_pair(V,M);
}
