#include "fun.h"

/*
Michał Suchorowski
Two cold atoms in harmonic trap
version: 02.02.2022

Declarations of class State and functions are included in header file "fun.h"
Their defintions are in "*.cpp" files, right file is pointed out in the header files and in main.cpp when function is used

*/

//functions run_* in calculations.cpp !

int main(int argc, char** argv){

 // double B=atof(argv[1]);
  double s=3;
  int basis_size=30;
 // int ind=atoi(argv[1]); 
 // int ind_last=atoi(argv[2]);
  
 // cout << ind << endl;
  double m1=1,m2=2;

//g(S,J,L,I,F,mu);
  double g_Cr=g(3,3,0,0,3,0);
  double g_Dy=g(2,8,6,0,8,0);
   double g_Er=g(1,6,5,0,6,0);

  double f_Cr=3;
  double f_Dy=8;
  double f_Er=6;

  //run_time_evolution_product_state_real_atoms(f_Cr, f_Er, basis_size);

  cout << g_Cr << " " << g_Dy << " " << g_Er << endl;
 // if(ind_last!=2999)
 //	 run_time_evolution_product_state_real_atoms_coeff(0, 1, 1, g_Cr, g_Er, f_Cr, f_Er, basis_size, m1, m2, 1,ind,ind_last);
 // run_time_evolution_product_state_real_atoms_eigen(0, 1, 1, g_Cr, g_Er, f_Cr, f_Er, basis_size, m1, m2, 1,ind);

//  run_time_evolution_product_state_real_atoms_coeff(0, 1, 1, g_Cr, g_Dy, f_Cr, f_Dy, basis_size, m1, m2, 1,ind);

 // run_phase_diagram(30,3,3,-0.025,0.2,200,B,B,200);

 // run_phase_diagram_Mx_Bx(5,3,3,-0.1,0.2,60,0,0.5,60);
  
  
  
  
  //run_time_evolution_product_state(0.5,2,s,s,basis_size,m1,m2,1);

 // run_energy_spectrum_J(5, s, s, 1, 1);
 // run_energy_spectrum_J(10, s, s, 1, 1);
 // run_energy_spectrum_J(10, s, s, 1, 0);

  /*double Bz;
  double Bx;
  double J=1;



  Bz=0;
  Bx=1;
  double s1,s2,g1,g2;
  s1=3;
  s2=4;
  g1=2;
  g2=2.5;*/

 // run_time_evolution_product_state_real_atoms(Bz, Bx, J, g1, g2, s1, s2, 10, 1, 2, 1);
  
 /* Bz=0;
  Bx=1;
  run_time_evolution_product_state(Bz, Bx, J, s, s, 10, -1, -2, 1);

  Bz=0;
  Bx=1;
  run_time_evolution_product_state(Bz, Bx, J, s, s, 10, -1, -2, 1);

  Bz=1;
  Bx=1;
  run_time_evolution_product_state(Bz, Bx, J, s, s, 10, -1, -2, 1);

  Bz=0.5;
  Bx=1;
  run_time_evolution_product_state(Bz, Bx, J, s, s, 10, -1, -2, 1);

  Bz=1;
  Bx=0.5;
  run_time_evolution_product_state(Bz, Bx, J, s, s, 10, -1, -2, 1);
  */

 // for(double J=0; J<=2; J+=0.2)
 //run_time_evolution_H0(0,1,2,4,2.5,2.5,10,4);
  

  //run_time_evolution_occupation_H0(B,B,J,J,s,s,basis_size,0);

  //for(int M=-6; M<=6; M++)
    //run_time_evolution_occupation_H0(B0,B0*k,J0,J0*sqrt(k),3,3,30,M);
      //run_time_evolution_occupation_H0(0,0.25,0,0.5,3,3,30,0);

//  run_time_evolution_occupation_H0(0,0,0.01,,1,1,30,0);


//  run_time_evolution_H0(0, 0.5, 0.5, 0.5, 3, 3, 10, 3);
 // run_time_evolution_H0(0, 0.5, 0.5, 0.5, 3, 3, 10, 3);
 // run_time_evolution_H0(0, 0.5, 0.5, 0.5, 3, 3, 10, 0);



  //run_time_evolution_occupation_H0(0,0.0,0,0.01,3,3,30,0);
  //run_time_evolution_occupation_H0(0,0.0,0,0.1,3,3,30,0);
  //run_time_evolution_occupation_H0(0,0.0,0,0.5,3,3,30,0);


  //run_time_evolution_occupation_H0(0.0,0.0,0.05,0.05*sqrt(10),3,3,30,0);
  //run_time_evolution_occupation_H0(0.0,0.0,0.05,0.05*sqrt(20),3,3,30,0);
 
  //run_time_evolution_occupation_H0(0.0,0.0,0.05,0.05*sqrt(100),3,3,10,0);
  //for(double J=0; J<0.0001; J+=0.000000000001)

//  for(double i=-1; i<=4; i+=0.1)
//    run_time_evolution_occupation_H0(-1,0.0,-pow(10,-i),-pow(10,-i)*sqrt(10),3,3,10,0);
  
 // run_time_evolution_occupation_H0(0,0,-0.2,0.2,3,3,30,1);

//  for(double i=4; i>=-1; i-=0.1)
//    run_time_evolution_occupation_H0(-1,0.0,pow(10,-i),pow(10,-i)*sqrt(10),3,3,10,0);

  
  //for(double J=-1; i<=8; i+=0.3)
 //   run_time_evolution_occupation_H0(-1,0.0,,-pow(10,-i)*sqrt(10),3,3,10,0);

  //k=1.5;
  //for(int M=-6; M<=6; M++)
  //  run_time_evolution_occupation_H0(B0,B0*k,J0,J0*sqrt(k),3,3,30,M);
  //run_time_evolution_occupation_H0(0,0.25,0,0.5,3,3,30,0);
  //run_time_evolution_occupation_H0(0,0.25,0,0.5,3,3,30,-2);

 // 
  //

  return 0;

}



