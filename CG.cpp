#include "fun.h"



// computation of Clebsch-Gordan coefficients
double CG_coff(double s1, double s2, double m1, double m2, double S, double M){
    if(M!=m1+m2) {
        //cout << "m1+m2!=M" << endl;
        return 0;}
    else if (s1!=s2 && abs(s1-s2) > S){
        return 0;
    }
    else{
        M=-M;
  
        double A = sqrt(gsl_sf_gamma(1+s1+s2-S)*gsl_sf_gamma(1+s1-s2+S)*gsl_sf_gamma(1-s1+s2+S)/gsl_sf_gamma(1+s1+s2+S+1));
        //cout << A << " ";
        double B = sqrt(gsl_sf_gamma(1+s1+m1)*gsl_sf_gamma(1+s1-m1)*gsl_sf_gamma(1+s2+m2)*gsl_sf_gamma(1+s2-m2)*gsl_sf_gamma(1+S-M)*gsl_sf_gamma(1+S+M));
        //cout << B << " ";

        double K=0;
        for(double k=0;k<=200;k++)
            if(  (s1+s2-S-k)>=0 &&  (s1-m1-k)>=0 && (s2+m2-k)>=0 && (S-s2+m1+k)>=0 && (S-s1-m2+k)>=0  )
                {
                K+= double(pow(-1,k+s1-s2+M))/(gsl_sf_gamma(1+k)*gsl_sf_gamma(1+s1+s2-S-k)*gsl_sf_gamma(1+s1-m1-k)*gsl_sf_gamma(1+s2+m2-k)*gsl_sf_gamma(1+S-s2+m1+k)*gsl_sf_gamma(1+S-s1-m2+k));
                }
       // cout << K << endl;
        double C=pow(-1,s1-s2+M)*sqrt(2*S+1)*(A*B*K);
        
        /*cout << "s1=" << s1 << " m1=" << m1;
        cout << " s2=" << s2 << " m2=" << m2;
        cout << " S=" << S << " M=" << M;
        cout << " C=" << C << endl;*/
        return C;
        
    }

}

// generate matrix from CG coefficients
gsl_matrix* CG_matrix(double s1, double s2, int N){

    int N_col=N;
    int N_row=(2*s1+1)*(2*s1+1);
    if(N_row!=N_col)
        cout << "Matrix is not rectangle" << endl;
    gsl_matrix* CG = gsl_matrix_calloc(N_row,N_col);

    int ind_coll=0;
    for(double M=s1*2; M>=-s1*2; M-=1)
        for(double S=s1*2; S>=abs(M); S-=1){
            int ind_row=0;
            for(double m1=-s1; m1<=s1; m1++) // sum over m1,m2 
                for(double m2=-s2; m2<=s2; m2++){
                    gsl_matrix_set(CG,ind_row,ind_coll,CG_coff(s1,s2,m1,m2,S,M));
                    ind_row++;
                    }
            ind_coll++;
        }

    return CG;

}

// generate inversion of CG matrix
gsl_matrix* inv_CG_matrix(double s1, double s2, int N){
    gsl_matrix* CG = CG_matrix(s1, s2, N);
    
    
    gsl_permutation *p = gsl_permutation_alloc(N);
    int s;

    // Compute the LU decomposition of this matrix
    gsl_linalg_LU_decomp(CG, p, &s);

    // Compute the  inverse of the LU decomposition
    gsl_matrix *inv = gsl_matrix_alloc(N, N);
    gsl_linalg_LU_invert(CG, p, inv);

    gsl_permutation_free(p);
    gsl_matrix_free(CG);

    return inv;
}

// rewrite real matrix to complex one
gsl_matrix_complex* MtoComplexM(gsl_matrix* M, int N){

    gsl_matrix_complex* CM = gsl_matrix_complex_alloc(N,N);
    for(int i=0; i<N; i++)
        for(int j=0; j<N; j++){
            double a=gsl_matrix_get(M,i,j);
            gsl_matrix_complex_set(CM,i,j,gsl_complex_rect(a,0));
        }

    return CM;
}