#include "fun.h"

///////////////////////////////////////////   ENERGY SPECTRUM //////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//energy spectrum as a function of J for different Bz
void run_energy_spectrum_J(int basis_size, double s1, double s2, bool S_conserved, bool M_conserved){
  double B[3]={0.0,1.0,0.2}; //,
  //for(double Bz=0.0; Bz<=0.5; Bz+=0.025){ // B/(omega*hbar)
  for(int Bi=0; Bi<3; Bi+=1){
    double Bz=B[Bi];
        cout << "Bz=" << Bz << endl;
  for(double J=-2; J<=2.00; J+=0.05){ //  J/(omega*hbar)
      J=roundf(J * 100) / 100;
      cout << "J=" << J << endl;

        if(S_conserved && M_conserved){
        for(int S=s1+s2; S>=0; S-=1){ //start from symmetric state
          for(int M=S; M>=-S; M-=1){
            vector <State> basis = generate_basis(s1,s2,basis_size,S_conserved,M_conserved,1,M,S);//generate basis states (state.cpp)
            gsl_matrix_complex* H =  generate_Hamiltonian(basis, J, Bz,0);//compute matrix elements and create Hamiltonian matrix (hamiltonian.cpp)
            pair<gsl_vector*, gsl_matrix_complex*> res = diagonalize_Hamiltonian(H,basis.size(),basis);//diagonalize Hamiltonian and find eigenvectors and eigenvalues (hamiltonian.cpp) 
            gsl_vector* Eigenvalues = res.first;
            gsl_matrix_complex* eigenvectors = res.second;
            string file_name = "data/E(J)_s="+to_string(s1)+"_Bz="+to_string(Bz*uB)+"Basis="+to_string(basis_size)+"_SM";
            ToFile_SM(file_name,Eigenvalues, eigenvectors, Bz*uB, J, S, M); // save energy to file (tofile.cpp)
            string state_file_name = "data/states/States(J)_s="+to_string(s1)+"_B="+to_string(Bz*uB);
            StateToFile(state_file_name, Bz*uB, J, s1, Eigenvalues, eigenvectors, basis); //save quantum numbers of the state to file assuming that state is a pure state (tofile.cpp)
            // Bz*uB in ToFile function to convert B/(omega*hbar) to uB*B/(omega*hbar)
          
            gsl_matrix_complex_free(H);
            gsl_vector_free(Eigenvalues);
            gsl_matrix_complex_free(eigenvectors);
          
          }
        }
        }
        else if(M_conserved){
            for(int M=s1+s2; M>=-(s1+s2); M-=1){
            vector <State> basis = generate_basis(s1,s2,basis_size,S_conserved,M_conserved,1,M);//generate basis states (state.cpp)
            gsl_matrix_complex* H =  generate_Hamiltonian(basis, J, 0,Bz);//compute matrix elements and create Hamiltonian matrix (hamiltonian.cpp)
            pair<gsl_vector*, gsl_matrix_complex*> res = diagonalize_Hamiltonian(H,basis.size(),basis);//diagonalize Hamiltonian and find eigenvectors and eigenvalues (hamiltonian.cpp) 
            gsl_vector* Eigenvalues = res.first;
            gsl_matrix_complex* eigenvectors = res.second;
            string file_name = "data_Bx/E(J)_s="+to_string(s1)+"_B="+to_string(Bz*uB)+"Basis="+to_string(basis_size)+"_M";
            ToFile_M(file_name,Eigenvalues, eigenvectors, Bz*uB, J, M,basis,s1); // save energy to file (tofile.cpp)
            string state_file_name = "data/states/States(J)_s="+to_string(s1)+"_B="+to_string(Bz*uB);
            StateToFile(state_file_name, Bz*uB, J, s1, Eigenvalues, eigenvectors, basis); //save quantum numbers of the state to file assuming that state is a pure state (tofile.cpp)
            // Bz*uB in ToFile function to convert B/(omega*hbar) to uB*B/(omega*hbar)
          
            gsl_matrix_complex_free(H);
            gsl_vector_free(Eigenvalues);
            gsl_matrix_complex_free(eigenvectors);
          
          }
        }
         else if(S_conserved){
            for(int S=s1+s2; S>=0; S-=1){
          //  if(S!=2) continue;
            vector <State> basis = generate_basis(s1,s2,basis_size,S_conserved,M_conserved,1,0,S);//generate basis states (state.cpp)
            gsl_matrix_complex* H =  generate_Hamiltonian(basis, J, 0,Bz);//compute matrix elements and create Hamiltonian matrix (hamiltonian.cpp)
            pair<gsl_vector*, gsl_matrix_complex*> res = diagonalize_Hamiltonian(H,basis.size(),basis);//diagonalize Hamiltonian and find eigenvectors and eigenvalues (hamiltonian.cpp) 
            gsl_vector* Eigenvalues = res.first;
            gsl_matrix_complex* eigenvectors = res.second;
            string file_name = "data/E(J)_s="+to_string(s1)+"_Bx="+to_string(Bz*uB)+"Basis="+to_string(basis_size)+"_S";
            ToFile_S(file_name,Eigenvalues, eigenvectors, Bz*uB, J, S,basis,s1); // save energy to file (tofile.cpp)
            string state_file_name = "data/states/States(J)_s="+to_string(s1)+"_Bx="+to_string(Bz*uB);
            // Bz*uB in ToFile function to convert B/(omega*hbar) to uB*B/(omega*hbar)
          
            gsl_matrix_complex_free(H);
            gsl_vector_free(Eigenvalues);
            gsl_matrix_complex_free(eigenvectors);
          
          }
        }
        else{
            vector <State> basis = generate_basis(s1,s2,basis_size,S_conserved,M_conserved,1);//generate basis states (state.cpp)
            gsl_matrix_complex* H =  generate_Hamiltonian(basis, J, 0, Bz);//compute matrix elements and create Hamiltonian matrix (hamiltonian.cpp)
            pair<gsl_vector*, gsl_matrix_complex*> res = diagonalize_Hamiltonian(H,basis.size(),basis);//diagonalize Hamiltonian and find eigenvectors and eigenvalues (hamiltonian.cpp) 
            gsl_vector* Eigenvalues = res.first;
            gsl_matrix_complex* eigenvectors = res.second;
            string file_name = "data_Bx/E(J)_s="+to_string(s1)+"_B="+to_string(Bz*uB)+"Basis="+to_string(basis_size);
            ToFile(file_name,Eigenvalues, eigenvectors, Bz*uB, J); // save energy to file (tofile.cpp)
            string state_file_name = "data/states/States(J)_s="+to_string(s1)+"_B="+to_string(Bz*uB);
            StateToFile(state_file_name, Bz*uB, J, s1, Eigenvalues, eigenvectors, basis); //save quantum numbers of the state to file assuming that state is a pure state (tofile.cpp)
          
            gsl_matrix_complex_free(H);
            gsl_vector_free(Eigenvalues);
            gsl_matrix_complex_free(eigenvectors);
        }

      }
  }
}

//energy spectrum as a function of B for different J
void run_energy_spectrum_B(int basis_size, double s1, double s2, bool S_conserved, bool M_conserved){

  double TabJ[3]={-0.5,0.,0.5}; //  J/(omega*hbar)
  for(int Ji=0; Ji<3; Ji+=1){
      //J=roundf(J * 100) / 100;
      double J=TabJ[Ji];
      cout << "J=" << J << endl;
      //double B[3]={0,0.025,0.25};
      //for(int Bi=0; Bi<3; Bi+=1){
        //double Bz=B[Bi];
      for(double Bz=0.0; Bz<=0.5; Bz+=0.0125){ // B/(omega*hbar)
        cout << "Bz=" << Bz << endl;

        if(S_conserved && M_conserved){
        for(double S=s1+s2; S>=0; S-=1){ //start from symmetric state
          for(double M=S; M>=-S; M-=1){
            vector <State> basis = generate_basis(s1,s2,basis_size,S_conserved,M_conserved,1,M,S);//generate basis states (state.cpp)
            gsl_matrix_complex* H =  generate_Hamiltonian(basis, J, Bz,0);//compute matrix elements and create Hamiltonian matrix (hamiltonian.cpp)
            pair<gsl_vector*, gsl_matrix_complex*> res = diagonalize_Hamiltonian(H,basis.size(),basis);//diagonalize Hamiltonian and find eigenvectors and eigenvalues (hamiltonian.cpp) 
            gsl_vector* Eigenvalues = res.first;
            gsl_matrix_complex* eigenvectors = res.second;
            string file_name = "data/E(Bz)_s="+to_string(s1)+"_J="+to_string(J)+"Basis="+to_string(basis_size)+"_SM";
            ToFile_SM(file_name,Eigenvalues, eigenvectors, Bz*uB, J, S, M); // save occupation to file (tofile.cpp)
            string state_file_name = "data/states/States(B)_s="+to_string(s1)+"_J="+to_string(J);
            StateToFile(state_file_name, Bz*uB, J, s1, Eigenvalues, eigenvectors, basis); //save quantum numbers of the state to file assuming that state is a pure state (tofile.cpp)
            // Bz*uB in ToFile function to convert B/(omega*hbar) to uB*B/(omega*hbar)
            
            gsl_matrix_complex_free(H);
            gsl_vector_free(Eigenvalues);
            gsl_matrix_complex_free(eigenvectors);

          }
        }
        }
        else if(M_conserved){
          for(double M=s1+s2; M>=-(s1+s2); M-=1){
            vector <State> basis = generate_basis(s1,s2,basis_size,S_conserved,M_conserved,1,M);//generate basis states (state.cpp)
            gsl_matrix_complex* H =  generate_Hamiltonian(basis, J, Bz,0);//compute matrix elements and create Hamiltonian matrix (hamiltonian.cpp)
            pair<gsl_vector*, gsl_matrix_complex*> res = diagonalize_Hamiltonian(H,basis.size(),basis);//diagonalize Hamiltonian and find eigenvectors and eigenvalues (hamiltonian.cpp) 
            gsl_vector* Eigenvalues = res.first;
            gsl_matrix_complex* eigenvectors = res.second;
            string file_name = "data/E(B)_s="+to_string(s1)+"_J="+to_string(J)+"Basis="+to_string(basis_size)+"_M";
            ToFile_M(file_name,Eigenvalues, eigenvectors, Bz*uB, J, M, basis, s1); // save occupation to file (tofile.cpp)
            string state_file_name = "data/states/States(B)_s="+to_string(s1)+"_J="+to_string(J);
            StateToFile(state_file_name, Bz*uB, J, s1, Eigenvalues, eigenvectors, basis); //save quantum numbers of the state to file assuming that state is a pure state (tofile.cpp)
            // Bz*uB in ToFile function to convert B/(omega*hbar) to uB*B/(omega*hbar)
            
            gsl_matrix_complex_free(H);
            gsl_vector_free(Eigenvalues);
            gsl_matrix_complex_free(eigenvectors);

          }
        }
        else if(S_conserved){
            for(int S=s1+s2; S>=0; S-=1){
            vector <State> basis = generate_basis(s1,s2,basis_size,S_conserved,M_conserved,1,0,S);//generate basis states (state.cpp)
            gsl_matrix_complex* H =  generate_Hamiltonian(basis, J, 0, Bz);//compute matrix elements and create Hamiltonian matrix (hamiltonian.cpp)
            pair<gsl_vector*, gsl_matrix_complex*> res = diagonalize_Hamiltonian(H,basis.size(),basis);//diagonalize Hamiltonian and find eigenvectors and eigenvalues (hamiltonian.cpp) 
            gsl_vector* Eigenvalues = res.first;
            gsl_matrix_complex* eigenvectors = res.second;
            string file_name = "data/E(Bx)_s="+to_string(s1)+"_J="+to_string(J)+"Basis="+to_string(basis_size)+"_M";
            ToFile_S(file_name,Eigenvalues, eigenvectors, Bz*uB, J, S,basis,s1); // save energy to file (tofile.cpp)
            // Bz*uB in ToFile function to convert B/(omega*hbar) to uB*B/(omega*hbar)
          
            gsl_matrix_complex_free(H);
            gsl_vector_free(Eigenvalues);
            gsl_matrix_complex_free(eigenvectors);
          
          }
        }
        else{
            vector <State> basis = generate_basis(s1,s2,basis_size,S_conserved,M_conserved,1);//generate basis states (state.cpp)
            gsl_matrix_complex* H =  generate_Hamiltonian(basis, J, Bz,0);//compute matrix elements and create Hamiltonian matrix (hamiltonian.cpp)
            pair<gsl_vector*, gsl_matrix_complex*> res = diagonalize_Hamiltonian(H,basis.size(),basis);//diagonalize Hamiltonian and find eigenvectors and eigenvalues (hamiltonian.cpp) 
            gsl_vector* Eigenvalues = res.first;
            gsl_matrix_complex* eigenvectors = res.second;
            string file_name = "data/E(B)_s="+to_string(s1)+"_J="+to_string(J)+"Basis="+to_string(basis_size);
            ToFile(file_name,Eigenvalues, eigenvectors, Bz*uB, J); // save occupation to file (tofile.cpp)
            string state_file_name = "data/states/States(B)_s="+to_string(s1)+"_J="+to_string(J);
            StateToFile(state_file_name, Bz*uB, J, s1, Eigenvalues, eigenvectors, basis); //save quantum numbers of the state to file assuming that state is a pure state (tofile.cpp)
            // Bz*uB in ToFile function to convert B/(omega*hbar) to uB*B/(omega*hbar)
            
            gsl_matrix_complex_free(H);
            gsl_vector_free(Eigenvalues);
            gsl_matrix_complex_free(eigenvectors);
        }
      }
    }
}

//////////////////////////////////////////////  MAGNETIZATION PHASE DIAGRAM  ///////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//calculate the magnetization (Mz) diagram for Bx magnetic field
void run_phase_diagram_Bx(int basis_size, double s1, double s2, double Jmin, double Jmax, int NJ, double Bmin, double Bmax, int NB){
  for(double J=Jmin; J<=Jmax; J+=(Jmax-Jmin)/NJ){ //  J/(omega*hbar)
  //for(double J=0;J<=2.1;J+=1){
      J=roundf(J * 10000) / 10000;
      cout << "J=" << J << endl;
      for(double Bx=Bmin; Bx<=Bmax; Bx+=(Bmax-Bmin)/NB){ // B/(omega*hbar)
      
      
        cout << "Bz=" << Bx << endl;

        double E=10000;
        int index=0; 
        double minS;
        double minM;
        double min_meanM=0;
        for(int S=0; S<=s1+s2; S++){
            vector <State> basis = generate_basis(s1,s2,basis_size,1,0,1,0,S);//generate basis states (state.cpp)
            gsl_matrix_complex* H =  generate_Hamiltonian(basis, J, 0,Bx);//compute matrix elements and create Hamiltonian matrix (hamiltonian.cpp)
            pair<gsl_vector*, gsl_matrix_complex*> res = diagonalize_Hamiltonian(H,basis.size(),basis);//diagonalize Hamiltonian and find eigenvectors and eigenvalues (hamiltonian.cpp) 
            gsl_vector* Eigenvalues = res.first;
            gsl_matrix_complex* eigenvectors = res.second;
            
          if(E>gsl_vector_min(Eigenvalues)){
              E=gsl_vector_min(Eigenvalues);
              index=gsl_vector_min_index(Eigenvalues);
              minS=S;
              double max_c=0;
              min_meanM=0;
              for(int i=0; i<basis_size*(S*2+1); i++){
                if(gsl_complex_abs2(gsl_matrix_complex_get(eigenvectors,i,index))>max_c){
                  minM=basis[i].M;
                  max_c=gsl_complex_abs2(gsl_matrix_complex_get(eigenvectors,i,index));
                  }
                min_meanM+=gsl_complex_abs2(gsl_matrix_complex_get(eigenvectors,i,index))*basis[i].M;
              }
              //minM=M;
            }

            gsl_matrix_complex_free(H);
            gsl_vector_free(Eigenvalues);
            gsl_matrix_complex_free(eigenvectors);

        }

        string file_name = "data/Phase_Basis_Bx_s="+to_string(s1)+"_"+to_string(NJ)+"x"+to_string(NB);
        ToFilePhase(file_name,Bx*uB,J,minS,min_meanM,minM);

      }
  }
}

//calculate the magnetization (Mx) diagram for Bx magnetic field
void run_phase_diagram_Mx_Bx(int basis_size, double s1, double s2, double Jmin, double Jmax, int NJ, double Bmin, double Bmax, int NB){
  for(double J=Jmin; J<=Jmax; J+=(Jmax-Jmin)/NJ){ //  J/(omega*hbar)
  //for(double J=0;J<=2.1;J+=1){
      J=roundf(J * 10000) / 10000;
      cout << "J=" << J << endl;
      for(double Bx=Bmin; Bx<=Bmax; Bx+=(Bmax-Bmin)/NB){ // B/(omega*hbar)
//      double Bz=Bmin;
      
        cout << "Bx=" << Bx << endl;

        double E=10000;
        int index=0; 
        double minS;
        double minM;
        gsl_complex meanMx=gsl_complex_rect(0,0);
        for(int S=0; S<=s1+s2; S++){
            vector <State> basis = generate_basis(s1,s2,basis_size,1,0,1,0,S);//generate basis states (state.cpp)
            gsl_matrix_complex* H =  generate_Hamiltonian(basis, J, 0,Bx);//compute matrix elements and create Hamiltonian matrix (hamiltonian.cpp)
            pair<gsl_vector*, gsl_matrix_complex*> res = diagonalize_Hamiltonian(H,basis.size(),basis);//diagonalize Hamiltonian and find eigenvectors and eigenvalues (hamiltonian.cpp) 
            gsl_vector* Eigenvalues = res.first;
            gsl_matrix_complex* eigenvectors = res.second;
            
          if(E>gsl_vector_min(Eigenvalues)){
              E=gsl_vector_min(Eigenvalues);
              index=gsl_vector_min_index(Eigenvalues);
              minS=S;
              double max_c=0;
              meanMx=gsl_complex_rect(0,0);
              for(int i=0; i<Eigenvalues->size; i++)
                for(int j=0; j<Eigenvalues->size; j++)
                if(basis[i].n==basis[j].n)
                {
                 double M=basis[i].M;
                 double Mp=basis[j].M;               
                 gsl_complex aa=gsl_complex_mul(gsl_matrix_complex_get(eigenvectors,i,index) , gsl_matrix_complex_get(eigenvectors,j,index)  );  
                 if(M==(Mp+1)) meanMx=gsl_complex_add(meanMx,gsl_complex_mul_real(aa,0.5*sqrt(S*(S+1)-Mp*(Mp+1))));
                 else if(M==(Mp-1)) meanMx=gsl_complex_add(meanMx,gsl_complex_mul_real(aa,0.5*sqrt(S*(S+1)-Mp*(Mp-1))));  
                }
                //  if(gsl_complex_abs2(gsl_matrix_complex_get(eigenvectors,i,index))>max_c){
                //    minM=basis[i].M;
                //    max_c=gsl_complex_abs2(gsl_matrix_complex_get(eigenvectors,i,index));
                //    }
                  
              
              //minM=M;
            }

            gsl_matrix_complex_free(H);
            gsl_vector_free(Eigenvalues);
            gsl_matrix_complex_free(eigenvectors);

        }

        string file_name = "data/Phase_Basis_Bx_s="+to_string(s1)+"_"+to_string(NJ)+"x"+to_string(NB);
        ToFilePhase(file_name,Bx*uB,J,minS,GSL_REAL(meanMx),minM);

      }
  }
}



//calculate the magnetization (Mz) diagram for Bz magnetic field
void run_phase_diagram_Bz(int basis_size, double s1, double s2, double Jmin, double Jmax, int NJ, double Bmin, double Bmax, int NB){
  for(double J=Jmin; J<=Jmax; J+=(Jmax-Jmin)/NJ){ //  J/(omega*hbar)
  //for(double J=0;J<=2.1;J+=1){
      J=roundf(J * 10000) / 10000;
      cout << "J=" << J << endl;
      for(double Bz=Bmin; Bz<=Bmax; Bz+=(Bmax-Bmin)/NB){ // B/(omega*hbar)
        //double Bz=Bmin;
      
        cout << "Bz=" << Bz << endl;

        double E=10000;
        int index=0; 
        double minS;
        double minM;
        for(int S=0; S<=s1+s2; S++)
        for(int M=S; M>=-S; M-=1){
            vector <State> basis = generate_basis(s1,s2,basis_size,1,1,1,M,S);//generate basis states (state.cpp)
            gsl_matrix_complex* H =  generate_Hamiltonian(basis, J, Bz,0);//compute matrix elements and create Hamiltonian matrix (hamiltonian.cpp)
            pair<gsl_vector*, gsl_matrix_complex*> res = diagonalize_Hamiltonian(H,basis.size(),basis);//diagonalize Hamiltonian and find eigenvectors and eigenvalues (hamiltonian.cpp) 
            gsl_vector* Eigenvalues = res.first;
            gsl_matrix_complex* eigenvectors = res.second;
            
          if(E>gsl_vector_min(Eigenvalues)){
              E=gsl_vector_min(Eigenvalues);
              index=gsl_vector_min_index(Eigenvalues);
              minS=S;
              minM=M;
            }

            gsl_matrix_complex_free(H);
            gsl_vector_free(Eigenvalues);
            gsl_matrix_complex_free(eigenvectors);

        }

        string file_name = "data/Phase_s="+to_string(s1)+"_"+to_string(NJ)+"x"+to_string(NB);
        ToFilePhase(file_name,Bz*uB,J,minS,minM);

    }
  }
}


/////////////////////////////////////////////////    TIME EVOLUTION      ///////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//NOTE: to calculate time evolution for two different atoms, use the functions with "different_atoms" in the name!

//calculate time evolution of product state (for two indistinguishable identical atoms)
void run_time_evolution_product_state(double Bz, double Bx, double J0, double s1, double s2, int basis_size, double m1, double m2, double k){
    
  //  double B=B0/k;
    double J=J0*pow(k,-0.5);
    cout << "Bz=" << Bz << " Bx=" << Bx << " J1=" << J0 <<  " basis=" << basis_size << " s1=" << s1 << " m1_init=" << m1 << " m2_init=" << m2 << endl;
    //vector<gsl_vector_complex> InitState = initial_state(m1_init,m2_init,n_init,s1,basis_size); //initial product state (time_ev.cpp)

    double E=10000;
    int index=0; 
 
    //rewrite found ground-state to form used when initial state was found in |m1m2> basis
    vector<gsl_vector_complex> InitState;
    for(int s=s1+s2; s>=0; s-=1){
        double N_m=0;
        N_m=2*s+1;
        gsl_vector_complex* state = gsl_vector_complex_calloc(basis_size*(N_m));
        for(int i=0; i<basis_size; i++){
          for(int m=-s; m<=s; m++){
            if(i<8) //CG_coff(s1,s2,m1,m2,s,m)
            { //cout  << m1 << " " << m2 << " " << s << " " << m << endl;
              gsl_vector_complex_set(state,i+(m+s)*basis_size,gsl_complex_rect(1/sqrt(8)*CG_coff(s1,s2,-m1,-m2,s,m),0));
            }
          }
        }
        
        InitState.push_back(*state);
    }
    cout << "Full vector" << endl;
    cout << "Basis size" << basis_size << endl;
    for(int i=0;i<=s1*2;i++){
      cout << "S=" << -i+s1*2 << " ";
        for(int j=0;j<InitState[i].size;j++)
            cout << GSL_REAL(gsl_vector_complex_get(&InitState[i],j)) << " ";
         cout << endl;   
        }

    ofstream file_M_comp;
    string file_name_Mcomp = "data/TimeMAnalysis";
    file_M_comp.open (file_name_Mcomp, std::ofstream::out|std::ofstream::app);
    file_M_comp << J << " ";
    cout << "Full vector" << endl;
    double sumM=0;
    for(int i=0;i<=s1*2;i++){
        for(int j=0;j<InitState[i].size;j++){
            cout << GSL_REAL(gsl_vector_complex_get(&InitState[i],j)) << " ";
            if(i==2) {
              sumM+=gsl_complex_abs2(gsl_vector_complex_get(&InitState[i],j));
              if((j+1)%20==0) {
                  cout << "Sum over n = " << sumM << endl;
                  file_M_comp << sumM << " ";
                  sumM=0;
              }
            }
          }
         cout << endl;   
        }
    file_M_comp << endl;
    file_M_comp.close();

   // return;

    //calculate eigenstates of final hamiltonian
    vector<gsl_vector> VectorEigenvalues; //vectors with eigenvalues and eigenvectors for each M state
    vector<gsl_matrix_complex> VectorEigenvectors;
    vector<vector<State>> VectorBasis;

    for(int S=s1+s2; S>=0; S-=1){
      cout << "S " << " " << S << endl;


      vector <State> basis = generate_basis(s1,s2,basis_size,1,0,1,0,S);//generate basis states for given S (state.cpp)
      VectorBasis.push_back(basis);
      //H_init -> B1, J1 (not needed for initial product state)
     
      //H_fin -> B2,J2
      gsl_matrix_complex* H_fin =  generate_Hamiltonian(basis, J, Bz, Bx);//compute matrix elements and create Hamiltonian matrix (hamiltonian.cpp)
      pair<gsl_vector*, gsl_matrix_complex*> res2 = diagonalize_Hamiltonian(H_fin,basis.size(),basis);//diagonalize Hamiltonian and find eigenvectors and eigenvalues (hamiltonian.cpp) 
      gsl_vector* Eigenvalues_fin = res2.first;
      VectorEigenvalues.push_back(*Eigenvalues_fin); //save eigenvalues to a vector
      gsl_matrix_complex* eigenvectors_fin = res2.second;  
      
      VectorEigenvectors.push_back(*eigenvectors_fin); //save eigenvectors to a vector   
      gsl_matrix_complex_free(H_fin);

        }
    
    cout << "time ev starting" << endl;
    //time evolution
    double dt=0.01; // 0.01 t in 2pi/omega
    double t0=0;
    double t1=1000;
        time_evolution_operator(InitState, VectorEigenvectors,VectorEigenvalues, t0, t1, dt, s1, basis_size, m1+m2, VectorBasis, Bz, Bx, J0, J0); //evolve (time_ev.cpp)
  
}

//calculate time evolution of the eigenstate of the initial Hamiltonian (ONLY for two indistinguishable identical atoms)
void run_time_evolution_H0(double B1, double B2, double J1, double J2, double s1, double s2, int basis_size, int S_init){
    
    cout << "B1=" << B1 << " J1=" << J1 << " B2=" << B2 << " J2=" << J2 <<  " basis=" << basis_size << " s1=" << s1 << "S=" << S_init << endl;
    //vector<gsl_vector_complex> InitState = initial_state(m1_init,m2_init,n_init,s1,basis_size); //initial product state (time_ev.cpp)

    string file_name = "data/TimeEv_LOG_S="+to_string(float(S_init))+"_s="+to_string(s1);
    ofstream file;
    file.open (file_name, std::ofstream::out|std::ofstream::app);
    file << "B1 B2 J1 J2 s1 s2 S_init" <<  endl;
    file << B1 << " " << B2  << " " << J1  << " " << J2  << " " << s1  << " " << s2  << " " << S_init << endl;
    file.close();

    double E=10000;
    int index=0; 
    double minS;
    double minM;

    //calculate groundstate of H_0 for given M=M_init
    //H_init -> B1, J1
    gsl_matrix_complex* eigenvectors_GroundState;
    //for(int M=s1+s2; M>=-(s1+s2); M-=1){
            //cout << "Init state M=" << M << endl;
            vector <State> basis_init = generate_basis(s1,s2,basis_size,1,0,1,0,S_init);//generate basis states (state.cpp)
            gsl_matrix_complex* H_init =  generate_Hamiltonian(basis_init, J1, 0,B1);//compute matrix elements and create Hamiltonian matrix (hamiltonian.cpp)
            pair<gsl_vector*, gsl_matrix_complex*> res_init = diagonalize_Hamiltonian(H_init,basis_init.size(),basis_init);//diagonalize Hamiltonian and find eigenvectors and eigenvalues (hamiltonian.cpp) 
            gsl_vector* Eigenvalues_init = res_init.first;
            gsl_matrix_complex* eigenvectors_init = res_init.second;
            
          if(E>gsl_vector_min(Eigenvalues_init)){
              E=gsl_vector_min(Eigenvalues_init);
              index=gsl_vector_min_index(Eigenvalues_init);
              minS=S_init;
              eigenvectors_GroundState = eigenvectors_init;
            }

            gsl_matrix_complex_free(H_init);
            gsl_vector_free(Eigenvalues_init);

        //}

    //rewrite found ground-state to form used when initial state was found in |m1m2> basis
    vector<gsl_vector_complex> InitState;
    for(int s=s1*2; s>=0; s-=1){
        double N_m=0;
        N_m=2*s+1;
        gsl_vector_complex* state = gsl_vector_complex_calloc(basis_size*(N_m));
        if(s==minS){
          for(int i=0; i<basis_size*N_m; i++){
              gsl_vector_complex_set(state,i,gsl_matrix_complex_get(eigenvectors_GroundState,i,index));
          }
        }
        InitState.push_back(*state);
    }
    cout << "Full vector" << endl;
    for(int i=0;i<s1*2;i++){
        for(int j=0;j<InitState[i].size;j++)
            cout << GSL_REAL(gsl_vector_complex_get(&InitState[i],j)) << " ";
         cout << endl;   
        }

    ofstream file_M_comp;
    string file_name_Mcomp = "data/TimeMAnalysis_S="+to_string(S_init);
    file_M_comp.open (file_name_Mcomp, std::ofstream::out|std::ofstream::app);
    file_M_comp << J1 << " ";
    cout << "Full vector" << endl;
    double sumM=0;
    for(int i=0;i<s1*2;i++){
        for(int j=0;j<InitState[i].size;j++){
            cout << GSL_REAL(gsl_vector_complex_get(&InitState[i],j)) << " ";
            if(i==2) {
              sumM+=gsl_complex_abs2(gsl_vector_complex_get(&InitState[i],j));
              if((j+1)%20==0) {
                  cout << "Sum over n = " << sumM << endl;
                  file_M_comp << sumM << " ";
                  sumM=0;
              }
            }
          }
         cout << endl;   
        }
    file_M_comp << endl;
    file_M_comp.close();

    //calculate eigenstates of final hamiltonian
    vector<gsl_vector> VectorEigenvalues; //vectors with eigenvalues and eigenvectors for each M state
    vector<gsl_matrix_complex> VectorEigenvectors;
    vector<vector<State>> VectorBasis;

    for(int S=s1+s2; S>=0; S-=1){
      cout << "S " << " " << S << endl;


      vector <State> basis = generate_basis(s1,s2,basis_size,1,0,1,0,S);//generate basis states for given S (state.cpp)
      
      VectorBasis.push_back(basis);
     
      //H_fin -> B2,J2
      gsl_matrix_complex* H_fin =  generate_Hamiltonian(basis, J2, 0,B2);//compute matrix elements and create Hamiltonian matrix (hamiltonian.cpp)
      pair<gsl_vector*, gsl_matrix_complex*> res2 = diagonalize_Hamiltonian(H_fin,basis.size(),basis);//diagonalize Hamiltonian and find eigenvectors and eigenvalues (hamiltonian.cpp) 
      gsl_vector* Eigenvalues_fin = res2.first;
      VectorEigenvalues.push_back(*Eigenvalues_fin); //save eigenvalues to a vector
      gsl_matrix_complex* eigenvectors_fin = res2.second;
      VectorEigenvectors.push_back(*eigenvectors_fin); //save eigenvectors to a vector

      gsl_matrix_complex_free(H_fin);

        }
    
    cout << "time ev starting" << endl;
    //time evolution
    double dt=0.01; // 0.01 t in 2pi/omega
    double t0=0;
    double t1=1000;
        time_evolution_operator(InitState, VectorEigenvectors,VectorEigenvalues, t0, t1, dt, s1, basis_size, S_init, VectorBasis, B1, B2, J1, J2); //evolve (time_ev.cpp)
  
}

/*NOTE: Time evolution for two different atoms is divided into parts:
1. Calculate the eigenstates and save them to file
2. Use eigenstates to calculate coefficients and save them to files
3. Use coefficients to calculate time evolution of the observables */

//calculate the eigenstates for two different atoms and save them to file
void run_time_evolution_product_state_different_atoms_eigen(double Bz, double Bx, double J, double g1, double g2, double s1, double s2, int basis_size, double m1, double m2, int ind){
    
 
    cout << "Bz=" << Bz << " Bx=" << Bx << " J=" << J <<  " basis=" << basis_size << " s1=" << s1 << " m1_init=" << m1 << " m2_init=" << m2 << endl;

    double E=10000;
    int index=0; 
 
    //rewrite found ground-state to form used when initial state was found in |m1m2> basis
    int counter=0;
    gsl_vector_complex* init_state = gsl_vector_complex_calloc(basis_size*(s1+s2+1)*(s1+s2+1));
    for(double s=s1+s2; s>=0; s-=1){
        double N_m=0;
        N_m=2*s+1;
        for(double m=-s; m<=s; m++){
          for(int i=0; i<basis_size; i++){
            if(i<8) //CG_coff(s1,s2,m1,m2,s,m)
            { 
            //  cout  << s << " " << m << " " << m1 << " " << m2 << " " <<  CG_coff(s1,s2,m1,m2,s,m) << endl;
              gsl_vector_complex_set(init_state,counter,gsl_complex_rect(1/sqrt(8)*CG_coff(s1,s2,-m1,-m2,s,m),0));
            }
            counter++;
          }
        }
      }
   // return;
    cout << "Full vector" << endl;
    cout << "Basis size" << basis_size << endl;
        for(int j=0;j<counter;j++)
            cout << GSL_REAL(gsl_vector_complex_get(init_state,j)) << " ";
         cout << endl;   
          
    vector <State> basis = generate_basis(s1,s2,basis_size,0,0,0);//generate basis states for given S (state.cpp)
      cout << "Basis generated!" << endl;
      
      gsl_matrix_complex* H_fin =  generate_Hamiltonian(basis, J, Bz, Bx,g1,g2);//compute matrix elements and create Hamiltonian matrix (hamiltonian.cpp)
      cout << "Hamiltonian generated!" << endl;
      pair<gsl_vector*, gsl_matrix_complex*> res2 = diagonalize_Hamiltonian(H_fin,basis.size(),basis);//diagonalize Hamiltonian and find eigenvectors and eigenvalues (hamiltonian.cpp) 
      cout << "Diagonalized" << endl;
      gsl_vector* Eigenvalues_fin = res2.first;
      gsl_matrix_complex* eigenvectors_fin = res2.second;
      cout << "Calculation done" << endl;    
      ToFileEigenstate(eigenvectors_fin,Eigenvalues_fin,"Eigenstate.dat","Eigenvalues.dat");
      gsl_matrix_complex_free(H_fin);
        
        
}

//read eigenstates from files and calculate the time evolution coeffciencts of the product state coefficients for two different atoms 
void run_time_evolution_product_state_different_atoms_coeff(double Bz, double Bx, double J, double g1, double g2, double s1, double s2, int basis_size, double m1, double m2, int ind, int ind_end){
    
    cout << "Bz=" << Bz << " Bx=" << Bx << " J=" << J <<  " basis=" << basis_size << " s1=" << s1 << " m1_init=" << m1 << " m2_init=" << m2 << endl;
    //vector<gsl_vector_complex> InitState = initial_state(m1_init,m2_init,n_init,s1,basis_size); //initial product state (time_ev.cpp)

    double E=10000;
    int index=0; 
 
    //rewrite found ground-state to form used when initial state was found in |m1m2> basis
    int counter=0;
    gsl_vector_complex* init_state = gsl_vector_complex_calloc(basis_size*(s1+s2+1)*(s1+s2+1));
    for(double s=s1+s2; s>=0; s-=1){
        double N_m=0;
        N_m=2*s+1;
        for(double m=-s; m<=s; m++){
          for(int i=0; i<basis_size; i++){
            if(i<8) //CG_coff(s1,s2,m1,m2,s,m)
            { 
            //  cout  << s << " " << m << " " << m1 << " " << m2 << " " <<  CG_coff(s1,s2,m1,m2,s,m) << endl;
              gsl_vector_complex_set(init_state,counter,gsl_complex_rect(1/sqrt(8)*CG_coff(s1,s2,-m1,-m2,s,m),0));
            }
            counter++;
          }
        }
      }
   // return;
    cout << "Full vector" << endl;
    cout << "Basis size" << basis_size << endl;
        for(int j=0;j<counter;j++)
            cout << GSL_REAL(gsl_vector_complex_get(init_state,j)) << " ";
         cout << endl;   
        
    vector <State> basis = generate_basis(s1,s2,basis_size,0,0,0);//generate basis states for given S (state.cpp)
      
    pair<gsl_vector*,gsl_matrix_complex*> res=FromFileEigenstate("Eigenstate.dat","Eigenvalues.dat", basis.size());
    gsl_vector* Eigenvalues_fin = res.first;
    gsl_matrix_complex* eigenvectors_fin = res.second;
        
        
    cout << "time ev starting" << endl;
    //time evolution
    double dt=0.01; // 0.01 t in 2pi/omega
    double t0=0;
    double t1=1000;
        time_evolution_operator_different_atoms_coeff(*init_state, *eigenvectors_fin,*Eigenvalues_fin, t0, t1, dt, s1, basis_size, m1+m2, basis, Bz, Bx, J, J, ind, ind_end); //evolve (time_ev.cpp)
  
}

//read coeff from files and calculate the time evolution
void run_time_evolution_product_state_different_atoms(double s1, double s2, int basis_size){
            
    cout << "time ev starting" << endl;
       
    vector <State> basis = generate_basis(s1,s2,basis_size,0,0,0);//generate basis states for given S (state.cpp)

    //time evolution
    double dt=0.01; // 0.01 t in 2pi/omega
    double t0=0;
    double t1=1000;
        //to change variable for which we want to calculate time evolution look to time_ev.cpp->time_evolution_m1(..) and change gsl_complex c=exp_value_m1 to exp_value_S or exp_value_M 
        time_evolution_operator_different_atoms(t0, t1, dt, basis, basis_size); //evolve (time_ev.cpp)
  
}


/////////////////////////////////////////   BASIS SIZE CONVERGENCE ///////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//calculate eigenstates and eigenenergies for different basis size
void run_basis_convergence(double s1, double s2, double J, double Bz){
   
   string file_name = "data/BasisSize_s1="+to_string(s1);


   for(int bs=1; bs<=150; bs++){
      cout << "bs=" << bs << endl;
      double minE;
      for(double S=s1+s2; S>=0; S-=1){ //start from symmetric state
          for(double M=S; M>=-S; M-=1){
            vector <State> basis = generate_basis(s1,s2,bs,0,0,1,M,S);//generate basis states (state.cpp)
            gsl_matrix_complex* H =  generate_Hamiltonian(basis, J, Bz,0);//compute matrix elements and create Hamiltonian matrix (hamiltonian.cpp)
            pair<gsl_vector*, gsl_matrix_complex*> res = diagonalize_Hamiltonian(H,basis.size(),basis);//diagonalize Hamiltonian and find eigenvectors and eigenvalues (hamiltonian.cpp) 
            gsl_vector* Eigenvalues = res.first; 
            double grE=gsl_vector_min(Eigenvalues); //find groundstate energy for given S,M
            if(bs==1 || grE<minE) //find true groundstate energy for whole basis
              minE=grE;

            gsl_matrix_complex_free(H);
            gsl_vector_free(Eigenvalues);
          }
        }
      ToFileBasisSize(file_name, bs*2, minE); //bsx2 because bs is a size of (anti)symmetric harmonic states

    }
}



