# Makefile # (C) 

##############################################################
### USTAWIENIA :

ARCHIVE := Suchorowski_Michal.tgz

##############################################################


PROG := eigen.exe

CXXFLAGS := -Wall -L/usr/local/lib -lgsl -lgslcblas -lm -I/usr/local/include 
CXX := g++
RM := rm -f
SRC = $(wildcard *.cpp)
OBJECTS = $(SRC:%.cpp=%.o)
CXXLINKLIBS := -L/usr/local/lib -lgsl -lgslcblas -lm -I/usr/local/include 
all : $(PROG)

$(PROG) : $(OBJECTS)
	$(CXX) $(OBJECTS) $(CXXLINKLIBS) -o $(PROG)

# include automatically generated dependencies:
-include $(SRC:%.cpp=%.d)

#$(ODIR)/%.o: $(SDIR)/%.cpp 
#    $(CC) -c $(INC) -o $@ $< $(CFLAGS) 
#where
#
#ODIR = object file output directory
#SDIR = .cpp source file directory
#INC = list of -I flags
#CFLAGS = the usual

test_makefile:
	echo $(OBJECTS)

run :
	./$(PROG)

test :
	valgrind ./$(PROG)

tgz :
	tar cvzf $(ARCHIVE) *.cpp *.h

clean:
	$(RM) *.o

cleanall:
	$(RM) *~ *.o *.d data/E* data/states/* $(PROG)

# end

cleantime:
	$(RM) *~ *.o *.d data_time/E* data_time/states/* Basis* $(PROG)

# end
